<!----------------------------------------------------------------------------->

# nacl/rule-list

#### A network ACL (NACL) rule that contains a list of **IPv4** and/or **IPv6** CIDR ranges

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//nacl/rule-list`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_vpc_core" {
  source    = "gitlab.com/bitservices/network/aws//vpc/core"
  name      = "sandpit01"
  owner     = var.owner
  company   = var.company
  ipv4_cidr = "192.168.188.0/23"
}

module "my_subnets_3_tier" {
  source                    =   "gitlab.com/bitservices/network/aws//subnets/3-tier"
  owner                     =   var.owner
  company                   =   var.company
  vpc                       =   module.my_vpc_core.name
  vpc_id                    =   module.my_vpc_core.id
  kubernetes_cluster        =   module.my_vpc_core.kubernetes_cluster
  availability_zones        = [ "a", "b"                                                                                       ]
  subnet_public_ipv4_cidrs  = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 1, 0), cidrsubnet(module.my_vpc_core.ipv4_cidr, 1, 1) ]
}

module "my_nacl_rule_list" {
  source     =   "gitlab.com/bitservices/network/aws//nacl/rule-list"
  port       =   80
  table      =   module.my_subnets_3_tier.nacl_public_table_id
  ipv4_cidrs = [ "1.2.3.4/32"                       ]
  ipv6_cidrs = [ "2001:db8:85a3::8a2e:370:7334/128" ]
}
```

<!----------------------------------------------------------------------------->
