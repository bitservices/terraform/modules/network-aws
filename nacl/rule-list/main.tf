################################################################################
# Required Variables
################################################################################

variable "port" {
  type        = number
  description = "The start port for the NACL rule."
}

variable "table" {
  type        = string
  description = "The NACL table ID to attach this set of rules to."
}

################################################################################
# Optional Variables
################################################################################

variable "action" {
  type        = string
  default     = "allow"
  description = "Action for the NACL rule."
}

variable "egress" {
  type        = bool
  default     = false
  description = "If 'true', signifies this is an egress NACL rule, otherwise this is an ingress NACL rule."
}

variable "to_port" {
  type        = number
  default     = null
  description = "Ending port for the NACL rule."
}

variable "protocol" {
  type        = string
  default     = "tcp"
  description = "Which protocol does this NACL rule apply to."
}

variable "start_number" {
  type        = number
  default     = 32000
  description = "The starting NACL rule number. The rule number is decremented for each CIDR in 'ipv4_cidrs' and 'ipv6_cidrs'."
}

################################################################################

variable "ipv4_cidrs" {
  type        = list(string)
  default     = []
  description = "A list of IPv4 CIDR ranges that this NACL rule should apply."
}

variable "ipv6_cidrs" {
  type        = list(string)
  default     = []
  description = "A list of IPv6 CIDR ranges that this NACL rule should apply."
}

################################################################################
# Locals
################################################################################

locals {
  to_port = coalesce(var.to_port, var.port)
}

################################################################################
# Resources
################################################################################

resource "aws_network_acl_rule" "ipv4" {
  count          = length(var.ipv4_cidrs)
  egress         = var.egress
  to_port        = local.to_port
  protocol       = var.protocol
  from_port      = var.port
  cidr_block     = var.ipv4_cidrs[count.index]
  rule_action    = var.action
  rule_number    = var.start_number - count.index
  network_acl_id = var.table
}

################################################################################

resource "aws_network_acl_rule" "ipv6" {
  count           = length(var.ipv6_cidrs)
  egress          = var.egress
  to_port         = local.to_port
  protocol        = var.protocol
  from_port       = var.port
  rule_action     = var.action
  rule_number     = var.start_number - length(var.ipv4_cidrs) - count.index
  network_acl_id  = var.table
  ipv6_cidr_block = var.ipv6_cidrs[count.index]
}

################################################################################
# Outputs
################################################################################

output "table" {
  value = var.table
}

################################################################################

output "ipv4_count" {
  value = length(var.ipv4_cidrs)
}

################################################################################

output "ipv6_count" {
  value = length(var.ipv6_cidrs)
}

################################################################################

output "ipv4_ids" {
  value = aws_network_acl_rule.ipv4.*.id
}

output "ipv4_port" {
  value = aws_network_acl_rule.ipv4.*.from_port
}

output "ipv4_action" {
  value = aws_network_acl_rule.ipv4.*.rule_action
}

output "ipv4_egress" {
  value = aws_network_acl_rule.ipv4.*.egress
}

output "ipv4_number" {
  value = aws_network_acl_rule.ipv4.*.rule_number
}

output "ipv4_to_port" {
  value = aws_network_acl_rule.ipv4.*.to_port
}

output "ipv4_protocol" {
  value = aws_network_acl_rule.ipv4.*.protocol
}

################################################################################

output "ipv6_ids" {
  value = aws_network_acl_rule.ipv6.*.id
}

output "ipv6_port" {
  value = aws_network_acl_rule.ipv6.*.from_port
}

output "ipv6_action" {
  value = aws_network_acl_rule.ipv6.*.rule_action
}

output "ipv6_egress" {
  value = aws_network_acl_rule.ipv6.*.egress
}

output "ipv6_number" {
  value = aws_network_acl_rule.ipv6.*.rule_number
}

output "ipv6_to_port" {
  value = aws_network_acl_rule.ipv6.*.to_port
}

output "ipv6_protocol" {
  value = aws_network_acl_rule.ipv6.*.protocol
}

################################################################################
