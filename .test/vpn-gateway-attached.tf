################################################################################

module "vpn_gateway_attached" {
  source  = "../vpn/gateway/attached"
  vpc     = module.vpc_core.name
  owner   = local.owner
  company = local.company
}

################################################################################
