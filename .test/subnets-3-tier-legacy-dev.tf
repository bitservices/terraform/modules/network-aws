################################################################################

module "subnets_3_tier_legacy_dev" {
  source                    = "../subnets/3-tier-legacy-dev"
  vpc                       = module.vpc_core.name
  vpc_id                    = module.vpc_core.id
  owner                     = local.owner
  company                   = local.company
  availability_zones        = ["a", "b"]
  subnet_public_ipv4_cidrs  = [cidrsubnet(module.vpc_core.ipv4_cidr, 3, 6), cidrsubnet(module.vpc_core.ipv4_cidr, 3, 7)]
  subnet_backend_ipv4_cidrs = [cidrsubnet(module.vpc_core.ipv4_cidr, 3, 4), cidrsubnet(module.vpc_core.ipv4_cidr, 3, 5)]
  subnet_private_ipv4_cidrs = [cidrsubnet(module.vpc_core.ipv4_cidr, 2, 0), cidrsubnet(module.vpc_core.ipv4_cidr, 2, 1)]
}

################################################################################
