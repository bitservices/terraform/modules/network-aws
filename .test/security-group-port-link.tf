################################################################################

module "security_group_port_link" {
  source            = "../security/port-link"
  vpc               = module.vpc_core.name
  flow              = "ingress"
  port              = "443"
  vpc_id            = module.vpc_core.id
  protocol          = "tcp"
  link_group_id     = module.security_group.id
  security_group_id = module.security_group.id
}

################################################################################
