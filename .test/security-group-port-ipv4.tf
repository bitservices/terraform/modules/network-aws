################################################################################

module "security_group_port_ipv4" {
  source            = "../security/port-ipv4"
  vpc               = module.vpc_core.name
  flow              = "ingress"
  port              = "443"
  vpc_id            = module.vpc_core.id
  protocol          = "tcp"
  security_group_id = module.security_group.id
}

################################################################################
