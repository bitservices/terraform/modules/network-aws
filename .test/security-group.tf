################################################################################

module "security_group" {
  source  = "../security/group"
  vpc     = module.vpc_core.name
  class   = "foobar"
  owner   = local.owner
  company = local.company
}

################################################################################
