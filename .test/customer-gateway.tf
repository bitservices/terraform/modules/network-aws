################################################################################

module "customer_gateway" {
  source  = "../vpn/customer"
  ip      = "5.6.7.8"
  site    = "London"
  owner   = local.owner
  company = local.company
}

################################################################################
