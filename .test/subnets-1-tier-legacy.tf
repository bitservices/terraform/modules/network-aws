################################################################################

module "subnets_1_tier_legacy" {
  source                   = "../subnets/1-tier-legacy"
  owner                    = local.owner
  company                  = local.company
  vpc                      = module.vpc_core.name
  vpc_id                   = module.vpc_core.id
  availability_zones       = ["a", "b"]
  subnet_public_ipv4_cidrs = [cidrsubnet(module.vpc_core.ipv4_cidr, 1, 0), cidrsubnet(module.vpc_core.ipv4_cidr, 1, 1)]
}

################################################################################
