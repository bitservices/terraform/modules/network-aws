################################################################################
# Required Variables
################################################################################

variable "ip" {
  type        = string
  description = "The public facing IP address of the customer gateway."
}

variable "site" {
  type        = string
  description = "The name of the customers site where this gateway is located."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "asn" {
  type        = number
  default     = 65000
  description = "The customer gateway BGP autonomous system number."
}

variable "type" {
  type        = string
  default     = "ipsec.1"
  description = "The customer gateway type."
}

################################################################################
# Locals
################################################################################

locals {
  name = format("%s-%s", lower(var.company), lower(replace(var.site, " ", "-")))
}

################################################################################
# Resources
################################################################################

resource "aws_customer_gateway" "scope" {
  bgp_asn    = var.asn
  ip_address = var.ip
  type       = var.type

  tags = {
    Name    = local.name
    Site    = var.site
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "site" {
  value = var.site
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "name" {
  value = local.name
}

################################################################################

output "id" {
  value = aws_customer_gateway.scope.id
}

output "ip" {
  value = aws_customer_gateway.scope.ip_address
}

output "asn" {
  value = aws_customer_gateway.scope.bgp_asn
}

output "type" {
  value = aws_customer_gateway.scope.type
}

################################################################################
