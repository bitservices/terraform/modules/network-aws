<!----------------------------------------------------------------------------->

# vpn/customer

#### Manage [VPN] customer gateway addresses

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//vpn/customer`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_customer_gateway" {
  source  = "gitlab.com/bitservices/network/aws//vpn/customer"
  ip      = "5.6.7.8"
  site    = "London"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[VPN]: https://aws.amazon.com/vpn/

<!----------------------------------------------------------------------------->
