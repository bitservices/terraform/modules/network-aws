################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this VPN gateway."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "asn" {
  type        = string
  default     = 64512
  description = "The AWS side VPN gateway BGP autonomous system number."
}

variable "zone" {
  type        = string
  default     = null
  description = "The availability zone for the VPN gateway (or blank for automatic). Specified as a single letter."
}

################################################################################
# Locals
################################################################################

locals {
  availability_zone = var.zone == null ? null : format("%s%s", data.aws_region.scope.name, var.zone)
}

################################################################################
# Resources
################################################################################

resource "aws_vpn_gateway" "scope" {
  amazon_side_asn   = var.asn
  availability_zone = local.availability_zone

  tags = {
    Name    = var.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "name" {
  value = var.name
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "asn" {
  value = var.asn
}

output "zone" {
  value = var.zone
}

################################################################################

output "availability_zone" {
  value = local.availability_zone
}

################################################################################

output "id" {
  value = aws_vpn_gateway.scope.id
}

################################################################################
