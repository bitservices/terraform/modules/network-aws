<!----------------------------------------------------------------------------->

# vpn/gateway/detached

#### [VPN] gateways where attachment state is not managed by this module

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//vpn/gateway/detached`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_vpn_gateway_detached" {
  source  = "gitlab.com/bitservices/network/aws//vpn/gateway/detached"
  name    = "foobar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[VPN]: https://aws.amazon.com/vpn/

<!----------------------------------------------------------------------------->
