################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "asn" {
  type        = string
  default     = 64512
  description = "The AWS side VPN gateway BGP autonomous system number."
}

variable "zone" {
  type        = string
  default     = null
  description = "The availability zone for the VPN gateway (or blank for automatic). Specified as a single letter."
}

variable "class" {
  type        = string
  default     = "default"
  description = "This forms the name of the VPN gateway. It is appended to the VPC name."
}

################################################################################
# Locals
################################################################################

locals {
  name              = format("%s-%s", var.vpc, var.class)
  availability_zone = var.zone == null ? null : format("%s%s", data.aws_region.scope.name, var.zone)
}

################################################################################
# Resources
################################################################################

resource "aws_vpn_gateway" "scope" {
  vpc_id            = data.aws_vpc.scope.id
  amazon_side_asn   = var.asn
  availability_zone = local.availability_zone

  tags = {
    VPC     = var.vpc
    Name    = local.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "asn" {
  value = var.asn
}

output "zone" {
  value = var.zone
}

output "class" {
  value = var.class
}

################################################################################

output "name" {
  value = local.name
}

output "availability_zone" {
  value = local.availability_zone
}

################################################################################

output "id" { value = aws_vpn_gateway.scope.id }

################################################################################
