<!----------------------------------------------------------------------------->

# vpn/gateway/attached

#### [VPN] gateways that are attached to a [VPC]

--------------------------------------------------------------------------------

**NOTE:** This is not normally desirable as its useful to keep [VPN] components
behind when a [VPC] is destroyed so it can be easily re-enabled if the [VPC] is
later rebuilt

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//vpn/gateway/attached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_vpn_gateway_attached" {
  source  = "gitlab.com/bitservices/network/aws//vpn/gateway/attached"
  vpc     = module.my_vpc_core.name
  owner   = var.owner
  company = var.company
}
```

<!----------------------------------------------------------------------------->

[VPC]: https://aws.amazon.com/vpc/
[VPN]: https://aws.amazon.com/vpn/

<!----------------------------------------------------------------------------->
