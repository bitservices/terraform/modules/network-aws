<!----------------------------------------------------------------------------->

# network (aws)

<!----------------------------------------------------------------------------->

## Description

Manages [AWS] networking components, such as [VPC], [VPN], subnets, etc.

<!----------------------------------------------------------------------------->

## Modules

* [nacl/rule-list](nacl/rule-list/README.md) - A network ACL (NACL) rule that contains a list of **IPv4** and/or **IPv6** CIDR ranges.
* [security/group](security/group/README.md) - A security group that can contain multiple rules.
* [security/port-ipv4](security/port-ipv4/README.md) - A single port or port range rule linked to a list of **IPv4** CIDR ranges.
* [security/port-ipv6](security/port-ipv6/README.md) - A single port or port range rule linked to a list of **IPv6** CIDR ranges.
* [security/port-link](security/port-link/README.md) - A single port or port range rule linked to another security group.
* [subnets/1-tier-legacy](subnets/1-tier-legacy/README.md) - The required components to form a 1-tier public-only subnet architecture on top of an existing [VPC] with full IPv4 support and optional [Kubernetes] support.
* [subnets/3-tier](subnets/3-tier/README.md) - The required components to form a 3-tier subnet architecture on top of an existing [VPC] with optional [Kubernetes] support.
* [subnets/3-tier-legacy](subnets/3-tier-legacy/README.md) - The required components to form a 3-tier subnet architecture on top of an existing [VPC] with full IPv4 support and optional [Kubernetes] support.
* [subnets/3-tier-legacy-dev](subnets/3-tier-legacy-dev/README.md) - The required components to form a 3-tier development subnet architecture on top of an existing [VPC] with full IPv4 support and optional [Kubernetes] support.
* [vpc/core](vpc/core/README.md) - The core components of a [VPC] with optional [Kubernetes] support.
* [vpc/default](vpc/default/README.md) - Manage the *default* [VPC].
* [vpn/customer](vpn/customer/README.md) - Manage [VPN] customer gateway addresses.
* [vpn/gateway/attached](vpn/gateway/attached/README.md) - [VPN] gateways that are attached to a [VPC].
* [vpn/gateway/detached](vpn/gateway/detached/README.md) - [VPN] gateways where attachment state is not managed by this module.

<!----------------------------------------------------------------------------->

[AWS]:        https://aws.amazon.com/
[VPC]:        https://aws.amazon.com/vpc/
[VPN]:        https://aws.amazon.com/vpn/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
