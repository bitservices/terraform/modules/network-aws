################################################################################
# Required Variables
################################################################################

variable "subnet_public_ipv4_cidrs" {
  type        = list(string)
  description = "A list of IPv4 CIDR ranges to use for the public subnets. List must be the same length as the list 'availability_zones'."
}

################################################################################
# Optional Variables
################################################################################

variable "subnet_public_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the public set of subnets."
}

variable "subnet_public_tier" {
  type        = string
  default     = "public"
  description = "Tier identifier for the public set of subnets."
}

################################################################################

variable "subnet_public_public_ip" {
  type        = bool
  default     = true
  description = "Specify 'true' to indicate that instances launched into the public subnets should be assigned a public IP address."
}

variable "subnet_public_assign_ipv6" {
  type        = bool
  default     = true
  description = "Specify 'true' to give all network interfaces launched into the public subnets an IPv6 address by default."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_public_names     = formatlist("%s - %s - %s - %s", var.vpc, var.subnet_public_set, var.subnet_public_tier, var.availability_zones)
  subnet_public_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 0)

  subnet_public_tags_extra = local.kubernetes_enabled ? {
    "kubernetes.io/role/elb"                                     = "1"
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "public" {
  count                           = length(var.availability_zones)
  vpc_id                          = data.aws_vpc.scope.id
  cidr_block                      = var.subnet_public_ipv4_cidrs[count.index]
  ipv6_cidr_block                 = cidrsubnet(local.subnet_public_ipv6_cidr, 4, count.index)
  availability_zone               = format("%s%s", data.aws_region.scope.name, var.availability_zones[count.index])
  map_public_ip_on_launch         = var.subnet_public_public_ip
  assign_ipv6_address_on_creation = var.subnet_public_assign_ipv6

  tags = merge(
    {
      "Set"        = var.subnet_public_set
      "VPC"        = var.vpc
      "IPv6"       = var.subnet_public_assign_ipv6
      "Name"       = local.subnet_public_names[count.index]
      "Tier"       = var.subnet_public_tier
      "Zone"       = var.availability_zones[count.index]
      "Owner"      = var.owner
      "Region"     = data.aws_region.scope.name
      "Company"    = var.company
      "PublicIP"   = var.subnet_public_public_ip
      "SubnetType" = title(var.subnet_public_tier)
    },
    local.subnet_public_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "assoc_public" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

################################################################################
# Outputs
################################################################################

output "subnet_public_set" {
  value = var.subnet_public_set
}

output "subnet_public_tier" {
  value = var.subnet_public_tier
}

################################################################################

output "subnet_public_public_ip" {
  value = var.subnet_public_public_ip
}

output "subnet_public_assign_ipv6" {
  value = var.subnet_public_assign_ipv6
}

################################################################################

output "subnet_public_names" {
  value = local.subnet_public_names
}

output "subnet_public_ipv6_cidr" {
  value = local.subnet_public_ipv6_cidr
}

output "subnet_public_tags_extra" {
  value = local.subnet_public_tags_extra
}

################################################################################

output "subnet_public_ids" {
  value = aws_subnet.public.*.id
}

output "subnet_public_zones" {
  value = aws_subnet.public.*.availability_zone
}

output "subnet_public_ipv4_cidrs" {
  value = aws_subnet.public.*.cidr_block
}

output "subnet_public_ipv6_cidrs" {
  value = aws_subnet.public.*.ipv6_cidr_block
}

################################################################################

output "subnet_public_route_table_associations" {
  value = aws_route_table_association.assoc_public.*.id
}

################################################################################
