################################################################################
# Optional Variables
################################################################################

variable "endpoint_s3_prefix" {
  type        = string
  default     = "com.amazonaws"
  description = "The S3 endpoint service address prefix. This should not be changed."
}

variable "endpoint_s3_service" {
  type        = string
  default     = "s3"
  description = "The S3 endpoint service name. This should not be changed."
}

################################################################################
# Locals
################################################################################

locals {
  endpoint_s3_service_name = format("%s.%s.%s", var.endpoint_s3_prefix, data.aws_region.scope.name, var.endpoint_s3_service)
}

################################################################################
# Resources
################################################################################

resource "aws_vpc_endpoint" "scope" {
  vpc_id          = data.aws_vpc.scope.id
  service_name    = local.endpoint_s3_service_name
  route_table_ids = tolist([aws_route_table.private.id])
}

################################################################################
# Outputs
################################################################################

output "endpoint_s3_prefix" {
  value = var.endpoint_s3_prefix
}

output "endpoint_s3_service" {
  value = var.endpoint_s3_service
}

################################################################################

output "endpoint_s3_service_name" {
  value = local.endpoint_s3_service_name
}

################################################################################

output "endpoint_s3_id" {
  value = aws_vpc_endpoint.scope.id
}

output "endpoint_s3_cidr_blocks" {
  value = aws_vpc_endpoint.scope.cidr_blocks
}

output "endpoint_s3_prefix_list_id" {
  value = aws_vpc_endpoint.scope.prefix_list_id
}

################################################################################
