################################################################################
# Optional Variables
################################################################################

variable "nat_gateway_set" {
  type        = string
  default     = "default"
  description = "Name of the NAT gateway set."
}

variable "nat_gateway_zone" {
  type        = string
  default     = null
  description = "The availability zone to place the single NAT gateway. Must be present in the 'availability_zones' list. If none is specified, the first item of the 'availability_zones' list is used by default."
}

################################################################################
# Locals
################################################################################

locals {
  nat_gateway_subnet_id = element(matchkeys(aws_subnet.public.*.id, aws_subnet.public.*.availability_zone, tolist([format("%s%s", data.aws_region.scope.name, local.nat_gateway_zone)])), 0)
  nat_gateway_name      = format("%s-%s-%s", var.vpc, var.nat_gateway_set, local.nat_gateway_zone)
  nat_gateway_zone      = coalesce(var.nat_gateway_zone, var.availability_zones[0])
}

################################################################################
# Resources
################################################################################

resource "aws_eip" "scope" {}

################################################################################

resource "aws_nat_gateway" "scope" {
  allocation_id = aws_eip.scope.id
  subnet_id     = local.nat_gateway_subnet_id

  tags = {
    VPC     = var.vpc
    Name    = local.nat_gateway_name
    Zone    = local.nat_gateway_zone
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  depends_on = [
    aws_internet_gateway.scope
  ]
}

################################################################################
# Outputs
################################################################################

output "nat_gateway_set" {
  value = var.nat_gateway_set
}

################################################################################

output "nat_gateway_name" {
  value = local.nat_gateway_name
}

output "nat_gateway_zone" {
  value = local.nat_gateway_zone
}

################################################################################

output "nat_gateway_id" {
  value = aws_nat_gateway.scope.id
}

output "nat_gateway_public_ip" {
  value = aws_nat_gateway.scope.public_ip
}

output "nat_gateway_subnet_id" {
  value = aws_nat_gateway.scope.subnet_id
}

output "nat_gateway_private_ip" {
  value = aws_nat_gateway.scope.private_ip
}

output "nat_gateway_interface_id" {
  value = aws_nat_gateway.scope.network_interface_id
}

output "nat_gateway_allocation_id" {
  value = aws_nat_gateway.scope.allocation_id
}

################################################################################
