################################################################################
# Optional Variables
################################################################################

variable "route_table_backend_class" {
  type        = string
  default     = "backend"
  description = "This forms the name of the backend route table. It is appended to the VPC name."
}

################################################################################
# Locals
################################################################################

locals {
  route_table_backend_name = format("%s-%s", var.vpc, var.route_table_backend_class)
  route_table_backend_zone = replace(join(" - ", aws_subnet.backend.*.availability_zone), data.aws_region.scope.name, "")
}

################################################################################
# Resources
################################################################################

resource "aws_route_table" "backend" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    VPC     = var.vpc
    Name    = local.route_table_backend_name
    Zone    = local.route_table_backend_zone
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "route_table_backend_class" {
  value = var.route_table_backend_class
}

################################################################################

output "route_table_backend_name" {
  value = local.route_table_backend_name
}

output "route_table_backend_zone" {
  value = local.route_table_backend_zone
}

################################################################################

output "route_table_backend_id" {
  value = aws_route_table.backend.id
}

################################################################################
