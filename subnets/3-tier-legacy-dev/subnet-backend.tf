################################################################################
# Required Variables
################################################################################

variable "subnet_backend_ipv4_cidrs" {
  type        = list(string)
  description = "A list of IPv4 CIDR ranges to use for the backend subnets. List must be the same length as the list 'availability_zones'."
}

################################################################################
# Optional Variables
################################################################################

variable "subnet_backend_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the backend set of subnets."
}

variable "subnet_backend_tier" {
  type        = string
  default     = "backend"
  description = "Tier identifier for the backend set of subnets."
}

################################################################################

variable "subnet_backend_public_ip" {
  type        = bool
  default     = false
  description = "Specify 'true' to indicate that instances launched into the backend subnets should be assigned a public IP address."
}

variable "subnet_backend_assign_ipv6" {
  type        = bool
  default     = true
  description = "Specify 'true' to give all network interfaces launched into the backend subnets an IPv6 address by default."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_backend_names     = formatlist("%s - %s - %s - %s", var.vpc, var.subnet_backend_set, var.subnet_backend_tier, var.availability_zones)
  subnet_backend_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 2)

  subnet_backend_tags_extra = local.kubernetes_enabled ? {
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "backend" {
  count                           = length(var.availability_zones)
  vpc_id                          = data.aws_vpc.scope.id
  cidr_block                      = var.subnet_backend_ipv4_cidrs[count.index]
  ipv6_cidr_block                 = cidrsubnet(local.subnet_backend_ipv6_cidr, 4, count.index)
  availability_zone               = format("%s%s", data.aws_region.scope.name, var.availability_zones[count.index])
  map_public_ip_on_launch         = var.subnet_backend_public_ip
  assign_ipv6_address_on_creation = var.subnet_backend_assign_ipv6

  tags = merge(
    {
      "Set"        = var.subnet_backend_set
      "VPC"        = var.vpc
      "IPv6"       = var.subnet_backend_assign_ipv6
      "Name"       = local.subnet_backend_names[count.index]
      "Tier"       = var.subnet_backend_tier
      "Zone"       = var.availability_zones[count.index]
      "Owner"      = var.owner
      "Region"     = data.aws_region.scope.name
      "Company"    = var.company
      "PublicIP"   = var.subnet_backend_public_ip
      "SubnetType" = title(var.subnet_backend_tier)
    },
    local.subnet_backend_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "assoc_backend" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.backend.*.id, count.index)
  route_table_id = aws_route_table.backend.id
}

################################################################################
# Outputs
################################################################################

output "subnet_backend_set" {
  value = var.subnet_backend_set
}

output "subnet_backend_tier" {
  value = var.subnet_backend_tier
}

################################################################################

output "subnet_backend_public_ip" {
  value = var.subnet_backend_public_ip
}

output "subnet_backend_assign_ipv6" {
  value = var.subnet_backend_assign_ipv6
}

################################################################################

output "subnet_backend_names" {
  value = local.subnet_backend_names
}

output "subnet_backend_ipv6_cidr" {
  value = local.subnet_backend_ipv6_cidr
}

output "subnet_backend_tags_extra" {
  value = local.subnet_backend_tags_extra
}

################################################################################

output "subnet_backend_ids" {
  value = aws_subnet.backend.*.id
}

output "subnet_backend_zones" {
  value = aws_subnet.backend.*.availability_zone
}

output "subnet_backend_ipv4_cidrs" {
  value = aws_subnet.backend.*.cidr_block
}

output "subnet_backend_ipv6_cidrs" {
  value = aws_subnet.backend.*.ipv6_cidr_block
}

################################################################################

output "subnet_backend_route_table_associations" {
  value = aws_route_table_association.assoc_backend.*.id
}

################################################################################
