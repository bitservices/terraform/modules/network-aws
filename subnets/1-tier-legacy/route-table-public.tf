################################################################################
# Optional Variables
################################################################################

variable "route_table_public_class" {
  type        = string
  default     = "public"
  description = "This forms the name of the public route table. It is appended to the VPC name."
}

variable "route_table_public_gateway_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "The IPv4 CIDR range that is permitted to be routed over the Internet gateway. Only applies to the 'public' route table."
}

variable "route_table_public_gateway_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "The IPv6 CIDR range that is permitted to be routed over the Internet gateway. Only applies to the 'public' route table."
}

################################################################################
# Locals
################################################################################

locals {
  route_table_public_name = format("%s-%s", var.vpc, var.route_table_public_class)
  route_table_public_zone = replace(join(" - ", aws_subnet.public.*.availability_zone), data.aws_region.scope.name, "")
}

################################################################################
# Resources
################################################################################

resource "aws_route_table" "public" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    VPC     = var.vpc
    Name    = local.route_table_public_name
    Zone    = local.route_table_public_zone
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################

resource "aws_route" "public_gateway_ipv4" {
  route_table_id         = aws_route_table.public.id
  gateway_id             = aws_internet_gateway.scope.id
  destination_cidr_block = var.route_table_public_gateway_ipv4_cidr
}

################################################################################

resource "aws_route" "public_gateway_ipv6" {
  route_table_id              = aws_route_table.public.id
  gateway_id                  = aws_internet_gateway.scope.id
  destination_ipv6_cidr_block = var.route_table_public_gateway_ipv6_cidr
}

################################################################################
# Outputs
################################################################################

output "route_table_public_class" {
  value = var.route_table_public_class
}

output "route_table_public_gateway_ipv4_cidr" {
  value = var.route_table_public_gateway_ipv4_cidr
}

output "route_table_public_gateway_ipv6_cidr" {
  value = var.route_table_public_gateway_ipv6_cidr
}

################################################################################

output "route_table_public_name" {
  value = local.route_table_public_name
}

output "route_table_public_zone" {
  value = local.route_table_public_zone
}

################################################################################

output "route_table_public_id" {
  value = aws_route_table.public.id
}

################################################################################

output "route_table_public_gateway_ipv4_id" {
  value = aws_route.public_gateway_ipv4.id
}

output "route_table_public_gateway_ipv6_id" {
  value = aws_route.public_gateway_ipv6.id
}

################################################################################
