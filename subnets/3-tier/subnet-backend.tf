################################################################################
# Optional Variables
################################################################################

variable "subnet_backend_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the backend set of subnets."
}

variable "subnet_backend_tier" {
  type        = string
  default     = "backend"
  description = "Tier identifier for the backend set of subnets."
}

################################################################################

variable "subnet_private_dns_hostname_type" {
  type        = string
  default     = "resource-name"
  description = "The type of hostnames to assign to instances in the private subnets at launch."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_backend_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 2)

  subnet_backend_names = {
    for availability_zone in var.availability_zones : availability_zone => format("%s - %s - %s - %s", var.vpc, var.subnet_backend_set, var.subnet_backend_tier, availability_zone)
  }

  subnet_backend_ipv6_cidrs = {
    for index, availability_zone in var.availability_zones : availability_zone => cidrsubnet(local.subnet_backend_ipv6_cidr, 4, index)
  }

  subnet_backend_tags_extra = local.kubernetes_enabled ? {
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "backend" {
  for_each                                       = toset(var.availability_zones)
  vpc_id                                         = data.aws_vpc.scope.id
  ipv6_native                                    = true
  ipv6_cidr_block                                = local.subnet_backend_ipv6_cidrs[each.value]
  availability_zone                              = format("%s%s", data.aws_region.scope.name, each.value)
  map_public_ip_on_launch                        = false
  assign_ipv6_address_on_creation                = true
  private_dns_hostname_type_on_launch            = var.subnet_private_dns_hostname_type
  enable_resource_name_dns_a_record_on_launch    = false
  enable_resource_name_dns_aaaa_record_on_launch = true

  tags = merge(
    {
      "Set"     = var.subnet_backend_set
      "VPC"     = var.vpc
      "Name"    = local.subnet_backend_names[each.value]
      "Tier"    = var.subnet_backend_tier
      "Zone"    = each.value
      "Owner"   = var.owner
      "Region"  = data.aws_region.scope.name
      "Company" = var.company
    },
    local.subnet_backend_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "backend" {
  for_each       = aws_subnet.backend
  subnet_id      = each.value.id
  route_table_id = aws_route_table.backend.id
}

################################################################################
# Outputs
################################################################################

output "subnet_backend_set" {
  value = var.subnet_backend_set
}

output "subnet_backend_tier" {
  value = var.subnet_backend_tier
}

################################################################################

output "subnet_backend_names" {
  value = local.subnet_backend_names
}

output "subnet_backend_ipv6_cidr" {
  value = local.subnet_backend_ipv6_cidr
}

output "subnet_backend_tags_extra" {
  value = local.subnet_backend_tags_extra
}

################################################################################

output "subnet_backend_ids" {
  value = [
    for backend_subnet in aws_subnet.backend : backend_subnet.id
  ]
}

output "subnet_backend_zones" {
  value = [
    for backend_subnet in aws_subnet.backend : backend_subnet.availability_zone
  ]
}

output "subnet_backend_ipv6_cidrs" {
  value = [
    for backend_subnet in aws_subnet.backend : backend_subnet.ipv6_cidr_block
  ]
}

################################################################################

output "subnet_backend_route_table_associations" {
  value = [
    for backend_route_table_association in aws_route_table_association.backend : backend_route_table_association.id
  ]
}

################################################################################
