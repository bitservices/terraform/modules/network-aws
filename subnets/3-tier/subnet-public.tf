################################################################################
# Required Variables
################################################################################

variable "subnet_public_ipv4_cidrs" {
  type        = list(string)
  description = "A list of IPv4 CIDR ranges to use for the public subnets. List must be the same length as the list of 'availability_zones'."

  validation {
    condition     = var.subnet_public_ipv4_cidrs == null || length(var.subnet_public_ipv4_cidrs) > 0
    error_message = "Public subnet IPv4 CIDRs cannot be null and must be a list of same length as supplied availability zones."
  }
}

################################################################################
# Optional Variables
################################################################################

variable "subnet_public_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the public set of subnets."
}

variable "subnet_public_tier" {
  type        = string
  default     = "public"
  description = "Tier identifier for the public set of subnets."
}

################################################################################

variable "subnet_public_public_ip" {
  type        = bool
  default     = true
  description = "Specify 'true' to indicate that instances launched into the public subnets should be assigned a public IPv4 address. Ignored if 'subnet_public_ipv4_cidrs' is not set."
}

variable "subnet_public_dns_hostname_type" {
  type        = string
  default     = "resource-name"
  description = "The type of hostnames to assign to instances in the public subnets at launch."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_public_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 0)
  subnet_public_public_ip = var.subnet_public_ipv4_cidrs == null ? false : var.subnet_public_public_ip

  subnet_public_names = {
    for availability_zone in var.availability_zones : availability_zone => format("%s - %s - %s - %s", var.vpc, var.subnet_public_set, var.subnet_public_tier, availability_zone)
  }

  subnet_public_ipv4_cidrs = {
    for index, availability_zone in var.availability_zones : availability_zone => var.subnet_public_ipv4_cidrs[index]
  }

  subnet_public_ipv6_cidrs = {
    for index, availability_zone in var.availability_zones : availability_zone => cidrsubnet(local.subnet_public_ipv6_cidr, 4, index)
  }

  subnet_public_tags_extra = local.kubernetes_enabled ? {
    "kubernetes.io/role/elb"                                     = "1"
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "public" {
  for_each                                       = toset(var.availability_zones)
  vpc_id                                         = data.aws_vpc.scope.id
  cidr_block                                     = local.subnet_public_ipv4_cidrs[each.value]
  ipv6_native                                    = false
  ipv6_cidr_block                                = local.subnet_public_ipv6_cidrs[each.value]
  availability_zone                              = format("%s%s", data.aws_region.scope.name, each.value)
  map_public_ip_on_launch                        = local.subnet_public_public_ip
  assign_ipv6_address_on_creation                = true
  private_dns_hostname_type_on_launch            = var.subnet_public_dns_hostname_type
  enable_resource_name_dns_a_record_on_launch    = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  tags = merge(
    {
      "Set"     = var.subnet_public_set
      "VPC"     = var.vpc
      "Name"    = local.subnet_public_names[each.value]
      "Tier"    = var.subnet_public_tier
      "Zone"    = each.value
      "Owner"   = var.owner
      "Region"  = data.aws_region.scope.name
      "Company" = var.company
    },
    local.subnet_public_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "public" {
  for_each       = aws_subnet.public
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id
}

################################################################################
# Outputs
################################################################################

output "subnet_public_set" {
  value = var.subnet_public_set
}

output "subnet_public_tier" {
  value = var.subnet_public_tier
}

################################################################################

output "subnet_public_public_ip" {
  value = local.subnet_public_public_ip
}

################################################################################

output "subnet_public_names" {
  value = local.subnet_public_names
}

output "subnet_public_ipv6_cidr" {
  value = local.subnet_public_ipv6_cidr
}

output "subnet_public_tags_extra" {
  value = local.subnet_public_tags_extra
}

################################################################################

output "subnet_public_ids" {
  value = [
    for public_subnet in aws_subnet.public : public_subnet.id
  ]
}

output "subnet_public_zones" {
  value = [
    for public_subnet in aws_subnet.public : public_subnet.availability_zone
  ]
}

output "subnet_public_ipv4_cidrs" {
  value = [
    for public_subnet in aws_subnet.public : public_subnet.cidr_block
  ]
}

output "subnet_public_ipv6_cidrs" {
  value = [
    for public_subnet in aws_subnet.public : public_subnet.ipv6_cidr_block
  ]
}

################################################################################

output "subnet_public_route_table_associations" {
  value = [
    for public_route_table_association in aws_route_table_association.public : public_route_table_association.id
  ]
}

################################################################################
