################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "availability_zones" {
  type        = list(string)
  description = "Set of availability zones the subnet tiers should cover."

  validation {
    condition     = var.availability_zones != null && length(var.availability_zones) > 0
    error_message = "Availability zones covered must not be null or empty."
  }
}

################################################################################
# Optional Variables
################################################################################

variable "kubernetes_shared" {
  type        = bool
  default     = true
  description = "A flag to specify if or not Kubernetes is sharing these subnets with other resources."
}

variable "kubernetes_cluster" {
  type        = string
  default     = null
  description = "The name of the Kubernetes cluster that will be using this architecture. No Kubernetes tags are set if this is omitted."
}

################################################################################
# Locals
################################################################################

locals {
  kubernetes_enabled = var.kubernetes_cluster != null
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "availability_zones" {
  value = var.availability_zones
}

output "availability_zone_count" {
  value = length(var.availability_zones)
}

################################################################################

output "kubernetes_shared" {
  value = var.kubernetes_shared
}

output "kubernetes_cluster" {
  value = var.kubernetes_cluster
}

output "kubernetes_enabled" {
  value = local.kubernetes_enabled
}

################################################################################
