################################################################################
# Optional Variables
################################################################################

variable "nacl_backend_ipv6_number" {
  type        = number
  default     = 32766
  description = "The rule number for the default IPv6 entries of the backend NACL. NACL entries are processed in ascending order by rule number."
}

################################################################################

variable "nacl_backend_class" {
  type        = string
  default     = "backend"
  description = "This forms the name of the backend NACL. It is appended to the VPC name."
}

################################################################################

variable "nacl_backend_rule_ingress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default ingress rule for the backend NACL table."
}

variable "nacl_backend_rule_ingress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default ingress rule for the 'backend' NACL table. '-1' for any protocol."
}

variable "nacl_backend_rule_ingress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default ingress rule for the backend NACL table."
}

variable "nacl_backend_rule_ingress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default ingress rule for the backend NACL table."
}

variable "nacl_backend_rule_ingress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default ingress rule for the backend NACL table."
}

################################################################################

variable "nacl_backend_rule_egress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default egress rule for the backend NACL table."
}

variable "nacl_backend_rule_egress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default egress rule for the 'backend' NACL table. '-1' for any protocol."
}

variable "nacl_backend_rule_egress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default egress rule for the backend NACL table."
}

variable "nacl_backend_rule_egress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default egress rule for the backend NACL table."
}

variable "nacl_backend_rule_egress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default egress rule for the backend NACL table."
}

################################################################################
# Locals
################################################################################

locals {
  nacl_backend_name = format("%s-%s", var.vpc, var.nacl_backend_class)
}

################################################################################
# Resources
################################################################################

resource "aws_network_acl" "backend_table" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    Name    = local.nacl_backend_name
    VPC     = var.vpc
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  subnet_ids = [
    for backend_subnet in aws_subnet.backend : backend_subnet.id
  ]
}

################################################################################

resource "aws_network_acl_rule" "backend_rule_ingress_ipv6" {
  egress          = false
  to_port         = var.nacl_backend_rule_ingress_port_to
  protocol        = var.nacl_backend_rule_ingress_protocol
  from_port       = var.nacl_backend_rule_ingress_port_from
  rule_action     = var.nacl_backend_rule_ingress_action
  rule_number     = var.nacl_backend_ipv6_number
  network_acl_id  = aws_network_acl.backend_table.id
  ipv6_cidr_block = var.nacl_backend_rule_ingress_ipv6_cidr
}

################################################################################

resource "aws_network_acl_rule" "backend_rule_egress_ipv6" {
  egress          = true
  to_port         = var.nacl_backend_rule_egress_port_to
  protocol        = var.nacl_backend_rule_egress_protocol
  from_port       = var.nacl_backend_rule_egress_port_from
  rule_action     = var.nacl_backend_rule_egress_action
  rule_number     = var.nacl_backend_ipv6_number
  network_acl_id  = aws_network_acl.backend_table.id
  ipv6_cidr_block = var.nacl_backend_rule_egress_ipv6_cidr
}

################################################################################
# Outputs
################################################################################

output "nacl_backend_ipv6_number" {
  value = var.nacl_backend_ipv6_number
}

################################################################################

output "nacl_backend_class" {
  value = var.nacl_backend_class
}

################################################################################

output "nacl_backend_rule_ingress_action" {
  value = var.nacl_backend_rule_ingress_action
}

output "nacl_backend_rule_ingress_protocol" {
  value = var.nacl_backend_rule_ingress_protocol
}

output "nacl_backend_rule_ingress_ipv6_cidr" {
  value = var.nacl_backend_rule_ingress_ipv6_cidr
}

output "nacl_backend_rule_ingress_port_from" {
  value = var.nacl_backend_rule_ingress_port_from
}

output "nacl_backend_rule_ingress_port_to" {
  value = var.nacl_backend_rule_ingress_port_to
}

################################################################################

output "nacl_backend_rule_egress_action" {
  value = var.nacl_backend_rule_egress_action
}

output "nacl_backend_rule_egress_protocol" {
  value = var.nacl_backend_rule_egress_protocol
}

output "nacl_backend_rule_egress_ipv6_cidr" {
  value = var.nacl_backend_rule_egress_ipv6_cidr
}

output "nacl_backend_rule_egress_port_from" {
  value = var.nacl_backend_rule_egress_port_from
}

output "nacl_backend_rule_egress_port_to" {
  value = var.nacl_backend_rule_egress_port_to
}

################################################################################

output "nacl_backend_name" {
  value = local.nacl_backend_name
}

################################################################################

output "nacl_backend_table_id" {
  value = aws_network_acl.backend_table.id
}

################################################################################

output "nacl_backend_rule_ingress_ipv6_id" {
  value = aws_network_acl_rule.backend_rule_ingress_ipv6.id
}

################################################################################

output "nacl_backend_rule_egress_ipv6_id" {
  value = aws_network_acl_rule.backend_rule_egress_ipv6.id
}

################################################################################
