################################################################################
# Optional Variables
################################################################################

variable "nacl_public_ipv4_number" {
  type        = number
  default     = 32765
  description = "The rule number for the default IPv4 entries of the public NACL. NACL entries are processed in ascending order by rule number."
}

variable "nacl_public_ipv6_number" {
  type        = number
  default     = 32766
  description = "The rule number for the default IPv6 entries of the public NACL. NACL entries are processed in ascending order by rule number."
}

################################################################################

variable "nacl_public_class" {
  type        = string
  default     = "public"
  description = "This forms the name of the public NACL. It is appended to the VPC name."
}

################################################################################

variable "nacl_public_rule_ingress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default ingress rule for the public NACL table."
}

variable "nacl_public_rule_ingress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default ingress rule for the 'public' NACL table. '-1' for any protocol."
}

variable "nacl_public_rule_ingress_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "IPv4 CIDR range for the default ingress rule for the public NACL table."
}

variable "nacl_public_rule_ingress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default ingress rule for the public NACL table."
}

variable "nacl_public_rule_ingress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default ingress rule for the public NACL table."
}

variable "nacl_public_rule_ingress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default ingress rule for the public NACL table."
}

################################################################################

variable "nacl_public_rule_egress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default egress rule for the public NACL table."
}

variable "nacl_public_rule_egress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default egress rule for the 'public' NACL table. '-1' for any protocol."
}

variable "nacl_public_rule_egress_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "IPv4 CIDR range for the default egress rule for the public NACL table."
}

variable "nacl_public_rule_egress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default egress rule for the public NACL table."
}

variable "nacl_public_rule_egress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default egress rule for the public NACL table."
}

variable "nacl_public_rule_egress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default egress rule for the public NACL table."
}

################################################################################
# Locals
################################################################################

locals {
  nacl_public_name = format("%s-%s", var.vpc, var.nacl_public_class)
}

################################################################################
# Resources
################################################################################

resource "aws_network_acl" "public_table" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    Name    = local.nacl_public_name
    VPC     = var.vpc
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  subnet_ids = [
    for public_subnet in aws_subnet.public : public_subnet.id
  ]
}

################################################################################

resource "aws_network_acl_rule" "public_rule_ingress_ipv4" {
  egress         = false
  to_port        = var.nacl_public_rule_ingress_port_to
  protocol       = var.nacl_public_rule_ingress_protocol
  from_port      = var.nacl_public_rule_ingress_port_from
  cidr_block     = var.nacl_public_rule_ingress_ipv4_cidr
  rule_action    = var.nacl_public_rule_ingress_action
  rule_number    = var.nacl_public_ipv4_number
  network_acl_id = aws_network_acl.public_table.id
}

################################################################################

resource "aws_network_acl_rule" "public_rule_ingress_ipv6" {
  egress          = false
  to_port         = var.nacl_public_rule_ingress_port_to
  protocol        = var.nacl_public_rule_ingress_protocol
  from_port       = var.nacl_public_rule_ingress_port_from
  rule_action     = var.nacl_public_rule_ingress_action
  rule_number     = var.nacl_public_ipv6_number
  network_acl_id  = aws_network_acl.public_table.id
  ipv6_cidr_block = var.nacl_public_rule_ingress_ipv6_cidr
}

################################################################################

resource "aws_network_acl_rule" "public_rule_egress_ipv4" {
  egress         = true
  to_port        = var.nacl_public_rule_egress_port_to
  protocol       = var.nacl_public_rule_egress_protocol
  from_port      = var.nacl_public_rule_egress_port_from
  cidr_block     = var.nacl_public_rule_egress_ipv4_cidr
  rule_action    = var.nacl_public_rule_egress_action
  rule_number    = var.nacl_public_ipv4_number
  network_acl_id = aws_network_acl.public_table.id
}

################################################################################

resource "aws_network_acl_rule" "public_rule_egress_ipv6" {
  egress          = true
  to_port         = var.nacl_public_rule_egress_port_to
  protocol        = var.nacl_public_rule_egress_protocol
  from_port       = var.nacl_public_rule_egress_port_from
  rule_action     = var.nacl_public_rule_egress_action
  rule_number     = var.nacl_public_ipv6_number
  network_acl_id  = aws_network_acl.public_table.id
  ipv6_cidr_block = var.nacl_public_rule_egress_ipv6_cidr
}

################################################################################
# Outputs
################################################################################

output "nacl_public_ipv4_number" {
  value = var.nacl_public_ipv4_number
}

output "nacl_public_ipv6_number" {
  value = var.nacl_public_ipv6_number
}

################################################################################

output "nacl_public_class" {
  value = var.nacl_public_class
}

################################################################################

output "nacl_public_rule_ingress_action" {
  value = var.nacl_public_rule_ingress_action
}

output "nacl_public_rule_ingress_protocol" {
  value = var.nacl_public_rule_ingress_protocol
}

output "nacl_public_rule_ingress_ipv4_cidr" {
  value = var.nacl_public_rule_ingress_ipv4_cidr
}

output "nacl_public_rule_ingress_ipv6_cidr" {
  value = var.nacl_public_rule_ingress_ipv6_cidr
}

output "nacl_public_rule_ingress_port_from" {
  value = var.nacl_public_rule_ingress_port_from
}

output "nacl_public_rule_ingress_port_to" {
  value = var.nacl_public_rule_ingress_port_to
}

################################################################################

output "nacl_public_rule_egress_action" {
  value = var.nacl_public_rule_egress_action
}

output "nacl_public_rule_egress_protocol" {
  value = var.nacl_public_rule_egress_protocol
}

output "nacl_public_rule_egress_ipv4_cidr" {
  value = var.nacl_public_rule_egress_ipv4_cidr
}

output "nacl_public_rule_egress_ipv6_cidr" {
  value = var.nacl_public_rule_egress_ipv6_cidr
}

output "nacl_public_rule_egress_port_from" {
  value = var.nacl_public_rule_egress_port_from
}

output "nacl_public_rule_egress_port_to" {
  value = var.nacl_public_rule_egress_port_to
}

################################################################################

output "nacl_public_name" {
  value = local.nacl_public_name
}

################################################################################

output "nacl_public_table_id" {
  value = aws_network_acl.public_table.id
}

################################################################################

output "nacl_public_rule_ingress_ipv4_id" {
  value = aws_network_acl_rule.public_rule_ingress_ipv4.id
}

output "nacl_public_rule_ingress_ipv6_id" {
  value = aws_network_acl_rule.public_rule_ingress_ipv6.id
}

################################################################################

output "nacl_public_rule_egress_ipv4_id" {
  value = aws_network_acl_rule.public_rule_egress_ipv4.id
}

output "nacl_public_rule_egress_ipv6_id" {
  value = aws_network_acl_rule.public_rule_egress_ipv6.id
}

################################################################################
