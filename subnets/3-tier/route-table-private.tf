################################################################################
# Optional Variables
################################################################################

variable "route_table_private_class" {
  type        = string
  default     = "private"
  description = "This forms the name of the private route table. It is appended to the VPC name."
}

variable "route_table_private_gateway_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "The IPv6 CIDR range that is permitted to be routed over the egress only Internet gateway. Only applies to the 'private' route table."
}

################################################################################
# Locals
################################################################################

locals {
  route_table_private_name = format("%s-%s", var.vpc, var.route_table_private_class)
  route_table_private_zone = replace(join(" - ", [for private_subnet in aws_subnet.private : private_subnet.availability_zone]), data.aws_region.scope.name, "")
}

################################################################################
# Resources
################################################################################

resource "aws_route_table" "private" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    VPC     = var.vpc
    Name    = local.route_table_private_name
    Zone    = local.route_table_private_zone
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################

resource "aws_route" "private_gateway_ipv6" {
  route_table_id              = aws_route_table.private.id
  egress_only_gateway_id      = aws_egress_only_internet_gateway.scope.id
  destination_ipv6_cidr_block = var.route_table_private_gateway_ipv6_cidr
}

################################################################################
# Outputs
################################################################################

output "route_table_private_class" {
  value = var.route_table_private_class
}

output "route_table_private_gateway_ipv6_cidr" {
  value = var.route_table_private_gateway_ipv6_cidr
}

################################################################################

output "route_table_private_name" {
  value = local.route_table_private_name
}

output "route_table_private_zone" {
  value = local.route_table_private_zone
}

################################################################################

output "route_table_private_id" {
  value = aws_route_table.private.id
}

################################################################################

output "route_table_private_gateway_ipv6_id" {
  value = aws_route.private_gateway_ipv6.id
}

################################################################################
