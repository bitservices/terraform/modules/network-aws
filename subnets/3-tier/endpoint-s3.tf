################################################################################
# Optional Variables
################################################################################

variable "endpoint_s3_create" {
  type        = bool
  default     = false
  description = "Should S3 endpoints be created for the private subnets?"
}

variable "endpoint_s3_prefix" {
  type        = string
  default     = "com.amazonaws"
  description = "The S3 endpoint service address prefix. This should not be changed."
}

variable "endpoint_s3_service" {
  type        = string
  default     = "s3"
  description = "The S3 endpoint service name. This should not be changed."
}

################################################################################
# Locals
################################################################################

locals {
  endpoint_s3_service_name = format("%s.%s.%s", var.endpoint_s3_prefix, data.aws_region.scope.name, var.endpoint_s3_service)
}

################################################################################
# Resources
################################################################################

resource "aws_vpc_endpoint" "s3" {
  for_each     = var.endpoint_s3_create ? toset([var.endpoint_s3_service]) : toset([])
  vpc_id       = data.aws_vpc.scope.id
  service_name = local.endpoint_s3_service_name

  route_table_ids = [
    for private_route_table in aws_route_table.private : private_route_table.id
  ]
}

################################################################################
# Outputs
################################################################################

output "endpoint_s3_create" {
  value = var.endpoint_s3_create
}

output "endpoint_s3_prefix" {
  value = var.endpoint_s3_prefix
}

output "endpoint_s3_service" {
  value = var.endpoint_s3_service
}

################################################################################

output "endpoint_s3_service_name" {
  value = local.endpoint_s3_service_name
}

################################################################################

output "endpoint_s3_id" {
  value = length(aws_vpc_endpoint.s3) == 1 ? aws_vpc_endpoint.s3[var.endpoint_s3_service].id : null
}

output "endpoint_s3_cidr_blocks" {
  value = length(aws_vpc_endpoint.s3) == 1 ? aws_vpc_endpoint.s3[var.endpoint_s3_service].cidr_blocks : null
}

output "endpoint_s3_prefix_list_id" {
  value = length(aws_vpc_endpoint.s3) == 1 ? aws_vpc_endpoint.s3[var.endpoint_s3_service].prefix_list_id : null
}

################################################################################
