################################################################################
# Optional Variables
################################################################################

variable "internet_gateway_class" {
  type        = string
  default     = "default"
  description = "This forms the name of the Internet gateway. It is appended to the VPC name."
}

################################################################################
# Locals
################################################################################

locals {
  internet_gateway_name = format("%s-%s", var.vpc, var.internet_gateway_class)
}

################################################################################
# Resources
################################################################################

resource "aws_internet_gateway" "scope" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    VPC     = var.vpc
    Name    = local.internet_gateway_name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "internet_gateway_class" {
  value = var.internet_gateway_class
}

################################################################################

output "internet_gateway_name" {
  value = local.internet_gateway_name
}

################################################################################

output "internet_gateway_id" {
  value = aws_internet_gateway.scope.id
}

################################################################################
