################################################################################
# Optional Variables
################################################################################

variable "subnet_private_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the private set of subnets."
}

variable "subnet_private_tier" {
  type        = string
  default     = "private"
  description = "Tier identifier for the private set of subnets."
}

################################################################################

variable "subnet_backend_dns_hostname_type" {
  type        = string
  default     = "resource-name"
  description = "The type of hostnames to assign to instances in the backend subnets at launch."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_private_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 1)

  subnet_private_names = {
    for availability_zone in var.availability_zones : availability_zone => format("%s - %s - %s - %s", var.vpc, var.subnet_private_set, var.subnet_private_tier, availability_zone)
  }

  subnet_private_ipv6_cidrs = {
    for index, availability_zone in var.availability_zones : availability_zone => cidrsubnet(local.subnet_private_ipv6_cidr, 4, index)
  }

  subnet_private_tags_extra = local.kubernetes_enabled ? {
    "kubernetes.io/role/internal-elb"                            = "1"
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "private" {
  for_each                                       = toset(var.availability_zones)
  vpc_id                                         = data.aws_vpc.scope.id
  ipv6_native                                    = true
  ipv6_cidr_block                                = local.subnet_private_ipv6_cidrs[each.value]
  availability_zone                              = format("%s%s", data.aws_region.scope.name, each.value)
  map_public_ip_on_launch                        = false
  assign_ipv6_address_on_creation                = true
  private_dns_hostname_type_on_launch            = var.subnet_backend_dns_hostname_type
  enable_resource_name_dns_a_record_on_launch    = false
  enable_resource_name_dns_aaaa_record_on_launch = true

  tags = merge(
    {
      "Set"     = var.subnet_private_set
      "VPC"     = var.vpc
      "Name"    = local.subnet_private_names[each.value]
      "Tier"    = var.subnet_private_tier
      "Zone"    = each.value
      "Owner"   = var.owner
      "Region"  = data.aws_region.scope.name
      "Company" = var.company
    },
    local.subnet_private_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "private" {
  for_each       = aws_subnet.private
  subnet_id      = each.value.id
  route_table_id = aws_route_table.private.id
}

################################################################################
# Outputs
################################################################################

output "subnet_private_set" {
  value = var.subnet_private_set
}

output "subnet_private_tier" {
  value = var.subnet_private_tier
}

################################################################################

output "subnet_private_names" {
  value = local.subnet_private_names
}

output "subnet_private_ipv6_cidr" {
  value = local.subnet_private_ipv6_cidr
}

output "subnet_private_tags_extra" {
  value = local.subnet_private_tags_extra
}

################################################################################

output "subnet_private_ids" {
  value = [
    for private_subnet in aws_subnet.private : private_subnet.id
  ]
}

output "subnet_private_zones" {
  value = [
    for private_subnet in aws_subnet.private : private_subnet.availability_zone
  ]
}

output "subnet_private_ipv6_cidrs" {
  value = [
    for private_subnet in aws_subnet.private : private_subnet.ipv6_cidr_block
  ]
}

################################################################################

output "subnet_private_route_table_associations" {
  value = [
    for private_route_table_association in aws_route_table_association.private : private_route_table_association.id
  ]
}

################################################################################
