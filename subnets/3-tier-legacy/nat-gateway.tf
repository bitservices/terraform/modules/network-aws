################################################################################
# Optional Variables
################################################################################

variable "nat_gateway_set" {
  type        = string
  default     = "default"
  description = "Name of the NAT gateway set."
}

################################################################################
# Locals
################################################################################

locals {
  nat_gateway_names = formatlist("%s-%s-%s", var.vpc, var.nat_gateway_set, var.availability_zones)
}

################################################################################
# Resources
################################################################################

resource "aws_eip" "scope" {
  count = length(var.availability_zones)
}

################################################################################

resource "aws_nat_gateway" "scope" {
  count         = length(var.availability_zones)
  allocation_id = element(aws_eip.scope.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)

  tags = {
    VPC     = var.vpc
    Name    = local.nat_gateway_names[count.index]
    Zone    = var.availability_zones[count.index]
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  depends_on = [
    aws_internet_gateway.scope
  ]
}

################################################################################
# Outputs
################################################################################

output "nat_gateway_set" {
  value = var.nat_gateway_set
}

################################################################################

output "nat_gateway_names" {
  value = local.nat_gateway_names
}

################################################################################

output "nat_gateway_ids" {
  value = aws_nat_gateway.scope.*.id
}

output "nat_gateway_public_ips" {
  value = aws_nat_gateway.scope.*.public_ip
}

output "nat_gateway_subnet_ids" {
  value = aws_nat_gateway.scope.*.subnet_id
}

output "nat_gateway_private_ips" {
  value = aws_nat_gateway.scope.*.private_ip
}

output "nat_gateway_interface_ids" {
  value = aws_nat_gateway.scope.*.network_interface_id
}

output "nat_gateway_allocation_ids" {
  value = aws_nat_gateway.scope.*.allocation_id
}

################################################################################
