################################################################################
# Required Variables
################################################################################

variable "subnet_private_ipv4_cidrs" {
  type        = list(string)
  description = "A list of IPv4 CIDR ranges to use for the private subnets. List must be the same length as the list 'availability_zones'."
}

################################################################################
# Optional Variables
################################################################################

variable "subnet_private_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the private set of subnets."
}

variable "subnet_private_tier" {
  type        = string
  default     = "private"
  description = "Tier identifier for the private set of subnets."
}

################################################################################

variable "subnet_private_public_ip" {
  type        = bool
  default     = false
  description = "Specify 'true' to indicate that instances launched into the private subnets should be assigned a public IP address."
}

variable "subnet_private_assign_ipv6" {
  type        = bool
  default     = true
  description = "Specify 'true' to give all network interfaces launched into the private subnets an IPv6 address by default."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_private_names     = formatlist("%s - %s - %s - %s", var.vpc, var.subnet_private_set, var.subnet_private_tier, var.availability_zones)
  subnet_private_ipv6_cidr = cidrsubnet(data.aws_vpc.scope.ipv6_cidr_block, 4, 1)

  subnet_private_tags_extra = local.kubernetes_enabled ? {
    "kubernetes.io/role/internal-elb"                            = "1"
    (format("kubernetes.io/cluster/%s", var.kubernetes_cluster)) = var.kubernetes_shared ? "shared" : "owned"
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_subnet" "private" {
  count                           = length(var.availability_zones)
  vpc_id                          = data.aws_vpc.scope.id
  cidr_block                      = var.subnet_private_ipv4_cidrs[count.index]
  ipv6_cidr_block                 = cidrsubnet(local.subnet_private_ipv6_cidr, 4, count.index)
  availability_zone               = format("%s%s", data.aws_region.scope.name, var.availability_zones[count.index])
  map_public_ip_on_launch         = var.subnet_private_public_ip
  assign_ipv6_address_on_creation = var.subnet_private_assign_ipv6

  tags = merge(
    {
      "Set"        = var.subnet_private_set
      "VPC"        = var.vpc
      "IPv6"       = var.subnet_private_assign_ipv6
      "Name"       = local.subnet_private_names[count.index]
      "Tier"       = var.subnet_private_tier
      "Zone"       = var.availability_zones[count.index]
      "Owner"      = var.owner
      "Region"     = data.aws_region.scope.name
      "Company"    = var.company
      "PublicIP"   = var.subnet_private_public_ip
      "SubnetType" = title(var.subnet_private_tier)
    },
    local.subnet_private_tags_extra
  )
}

################################################################################

resource "aws_route_table_association" "assoc_private" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

################################################################################
# Outputs
################################################################################

output "subnet_private_set" {
  value = var.subnet_private_set
}

output "subnet_private_tier" {
  value = var.subnet_private_tier
}

################################################################################

output "subnet_private_public_ip" {
  value = var.subnet_private_public_ip
}

output "subnet_private_assign_ipv6" {
  value = var.subnet_private_assign_ipv6
}

################################################################################

output "subnet_private_names" {
  value = local.subnet_private_names
}

output "subnet_private_ipv6_cidr" {
  value = local.subnet_private_ipv6_cidr
}

output "subnet_private_tags_extra" {
  value = local.subnet_private_tags_extra
}

################################################################################

output "subnet_private_ids" {
  value = aws_subnet.private.*.id
}

output "subnet_private_zones" {
  value = aws_subnet.private.*.availability_zone
}

output "subnet_private_ipv4_cidrs" {
  value = aws_subnet.private.*.cidr_block
}

output "subnet_private_ipv6_cidrs" {
  value = aws_subnet.private.*.ipv6_cidr_block
}

################################################################################

output "subnet_private_route_table_associations" {
  value = aws_route_table_association.assoc_private.*.id
}

################################################################################
