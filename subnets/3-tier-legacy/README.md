<!----------------------------------------------------------------------------->

# subnets/3-tier-legacy

#### The required components to form a 3-tier subnet architecture on top of an existing [VPC] with full IPv4 support and optional [Kubernetes] support

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//subnets/3-tier-legacy`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_vpc_core" {
  source    = "gitlab.com/bitservices/network/aws//vpc/core"
  name      = "sandpit01"
  owner     = var.owner
  company   = var.company
  ipv4_cidr = "192.168.188.0/23"
}

module "my_subnets_3_tier_legacy" {
  source                    =   "gitlab.com/bitservices/network/aws//subnets/3-tier-legacy"
  owner                     =   var.owner
  company                   =   var.company
  vpc                       =   module.my_vpc_core.name
  vpc_id                    =   module.my_vpc_core.id
  availability_zones        = [ "a", "b"                                                                                       ]
  subnet_public_ipv4_cidrs  = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 6), cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 7) ]
  subnet_backend_ipv4_cidrs = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 4), cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 5) ]
  subnet_private_ipv4_cidrs = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 2, 0), cidrsubnet(module.my_vpc_core.ipv4_cidr, 2, 1) ]
}
```

<!----------------------------------------------------------------------------->

[VPC]:        https://aws.amazon.com/vpc/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
