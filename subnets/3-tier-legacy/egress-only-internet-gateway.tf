################################################################################
# Resources
################################################################################

resource "aws_egress_only_internet_gateway" "scope" {
  vpc_id = data.aws_vpc.scope.id

  tags = {
    "VPC"     = var.vpc
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Company" = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "egress_only_internet_gateway_id" {
  value = aws_egress_only_internet_gateway.scope.id
}

################################################################################
