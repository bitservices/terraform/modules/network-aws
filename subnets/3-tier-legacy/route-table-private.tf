################################################################################
# Optional Variables
################################################################################

variable "route_table_private_class" {
  type        = string
  default     = "private"
  description = "This forms the name of the private route table. It is appended to the VPC name."
}

variable "route_table_private_gateway_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "The IPv4 CIDR range that is permitted to be routed over the NAT gateway(s). Only applies to the 'private' route table."
}

variable "route_table_private_gateway_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "The IPv6 CIDR range that is permitted to be routed over the NAT gateway(s). Only applies to the 'private' route table."
}

################################################################################
# Locals
################################################################################

locals {
  route_table_private_names = formatlist("%s-%s-%s", var.vpc, var.route_table_private_class, var.availability_zones)
}

################################################################################
# Resources
################################################################################

resource "aws_route_table" "private" {
  count  = length(var.availability_zones)
  vpc_id = data.aws_vpc.scope.id

  tags = {
    VPC     = var.vpc
    Name    = local.route_table_private_names[count.index]
    Zone    = var.availability_zones[count.index]
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################

resource "aws_route" "private_gateway_ipv4" {
  count                  = length(var.availability_zones)
  route_table_id         = aws_route_table.private.*.id[count.index]
  nat_gateway_id         = aws_nat_gateway.scope.*.id[count.index]
  destination_cidr_block = var.route_table_private_gateway_ipv4_cidr
}

################################################################################

resource "aws_route" "private_gateway_ipv6" {
  count                       = length(var.availability_zones)
  route_table_id              = aws_route_table.private.*.id[count.index]
  egress_only_gateway_id      = aws_egress_only_internet_gateway.scope.id
  destination_ipv6_cidr_block = var.route_table_private_gateway_ipv6_cidr
}

################################################################################
# Outputs
################################################################################

output "route_table_private_class" {
  value = var.route_table_private_class
}

output "route_table_private_gateway_ipv4_cidr" {
  value = var.route_table_private_gateway_ipv4_cidr
}

output "route_table_private_gateway_ipv6_cidr" {
  value = var.route_table_private_gateway_ipv6_cidr
}

################################################################################

output "route_table_private_names" {
  value = local.route_table_private_names
}

################################################################################

output "route_table_private_ids" {
  value = aws_route_table.private.*.id
}

################################################################################

output "route_table_private_gateway_ipv4_ids" {
  value = aws_route.private_gateway_ipv4.*.id
}

output "route_table_private_gateway_ipv6_ids" {
  value = aws_route.private_gateway_ipv6.*.id
}

################################################################################
