################################################################################
# Outputs
################################################################################

variable "link_group_id" {
  type        = string
  default     = null
  description = "The ID of the source (ingress) or target (egress) security group of this rule."
}

variable "link_group_name" {
  type        = string
  default     = null
  description = "The name of the source (ingress) or target (egress) security group of this rule. Ignored if 'link_group_id' is set."
}

variable "link_group_class" {
  type        = string
  default     = "perimeter"
  description = "The class of the source (ingress) or target (egress) security group of this rule. Ignored if 'security_group_name' is set."
}

################################################################################
# Locals
################################################################################

locals {
  link_group_name  = var.link_group_id == null ? coalesce(var.link_group_name, format("%s-%s", var.vpc, local.link_group_class)) : null
  link_group_class = var.link_group_name == null && var.link_group_id == null ? var.link_group_class : null
}

################################################################################
# Data Sources
################################################################################

data "aws_security_group" "link" {
  id     = var.link_group_id
  name   = local.link_group_name
  vpc_id = data.aws_vpc.scope.id
}

################################################################################
# Outputs
################################################################################

output "link_group_class" {
  value = local.link_group_class
}

################################################################################

output "link_group_id" {
  value = data.aws_security_group.link.id
}

output "link_group_arn" {
  value = data.aws_security_group.link.arn
}

output "link_group_name" {
  value = data.aws_security_group.link.name
}

output "link_group_tags" {
  value = data.aws_security_group.link.tags
}

output "link_group_description" {
  value = data.aws_security_group.link.description
}

################################################################################
