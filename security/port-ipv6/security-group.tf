################################################################################
# Outputs
################################################################################

variable "security_group_id" {
  type        = string
  default     = null
  description = "The ID of the security group to apply this rule to."
}

variable "security_group_name" {
  type        = string
  default     = null
  description = "The name of the security group to apply this rule to. Ignored if 'security_group_id' is set."
}

variable "security_group_class" {
  type        = string
  default     = "perimeter"
  description = "The class of the security group to apply this rule to. Ignored if 'security_group_name' is set."
}

################################################################################
# Locals
################################################################################

locals {
  security_group_name  = var.security_group_id == null ? coalesce(var.security_group_name, format("%s-%s", var.vpc, local.security_group_class)) : null
  security_group_class = var.security_group_name == null && var.security_group_id == null ? var.security_group_class : null
}

################################################################################
# Data Sources
################################################################################

data "aws_security_group" "scope" {
  id     = var.security_group_id
  name   = local.security_group_name
  vpc_id = data.aws_vpc.scope.id
}

################################################################################
# Outputs
################################################################################

output "security_group_class" {
  value = local.security_group_class
}

################################################################################

output "security_group_id" {
  value = data.aws_security_group.scope.id
}

output "security_group_arn" {
  value = data.aws_security_group.scope.arn
}

output "security_group_name" {
  value = data.aws_security_group.scope.name
}

output "security_group_tags" {
  value = data.aws_security_group.scope.tags
}

output "security_group_description" {
  value = data.aws_security_group.scope.description
}

################################################################################

