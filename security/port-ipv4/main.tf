################################################################################
# Required Variables
################################################################################

variable "flow" {
  type        = string
  description = "The type of rule being created. Can be 'ingress' or 'egress'."
}

variable "port" {
  type        = number
  description = "The start port (or ICMP type number if protocol is 'icmp')."
}

variable "protocol" {
  type        = string
  description = "The protocol. If not 'icmp', 'tcp', 'udp', or 'all' use the protocol number."
}

################################################################################
# Optional Variables
################################################################################

variable "to_port" {
  type        = number
  default     = null
  description = "The end port (or ICMP code if protocol is 'icmp')."
}

################################################################################

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of this security group rule."
}

################################################################################

variable "cidrs" {
  type        = list(string)
  default     = ["0.0.0.0/0"]
  description = "List of IPv4 CIDR blocks."
}

################################################################################
# Locals
################################################################################

locals {
  to_port = coalesce(var.to_port, var.port)
}

################################################################################
# Resources
################################################################################

resource "aws_security_group_rule" "scope" {
  type              = var.flow
  to_port           = local.to_port
  protocol          = var.protocol
  from_port         = var.port
  cidr_blocks       = var.cidrs
  description       = var.description
  security_group_id = data.aws_security_group.scope.id
}

################################################################################
# Outputs
################################################################################

output "cidrs" {
  value = var.cidrs
}

################################################################################

output "id" {
  value = aws_security_group_rule.scope.id
}

output "flow" {
  value = aws_security_group_rule.scope.type
}

output "port" {
  value = aws_security_group_rule.scope.from_port
}

output "to_port" {
  value = aws_security_group_rule.scope.to_port
}

output "protocol" {
  value = aws_security_group_rule.scope.protocol
}

output "description" {
  value = aws_security_group_rule.scope.description
}

################################################################################
