<!----------------------------------------------------------------------------->

# security/port-ipv4

#### A single port or port range rule linked to a list of **IPv4** CIDR ranges

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//security/port-ipv4`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_vpc_core" {
  source    = "gitlab.com/bitservices/network/aws//vpc/core"
  name      = "sandpit01"
  owner     = "terraform@bitservices.io"
  company   = "BITServices Ltd"
  ipv4_cidr = "192.168.188.0/23"
}

module "my_security_group" {
  source  = "gitlab.com/bitservices/network/aws//security/group"
  vpc     = module.my_vpc_core.name
  class   = "foobar"
  owner   = var.owner
  company = var.company
  vpc_id  = module.my_vpc_core.id
}

module "my_security_group_port_ipv4" {
  source            = "gitlab.com/bitservices/network/aws//security/port-ipv4"
  vpc               = module.my_vpc_core.name
  flow              = "ingress"
  port              = "443"
  vpc_id            = module.my_vpc_core.id
  protocol          = "tcp"
  security_group_id = module.my_security_group.id
}
```

<!----------------------------------------------------------------------------->
