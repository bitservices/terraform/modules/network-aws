<!----------------------------------------------------------------------------->

# security/group

#### A security group that can contain multiple rules

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//security/group`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_security_group" {
  source  = "gitlab.com/bitservices/network/aws//security/group"
  vpc     = "sandpit01"
  class   = "foobar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->
