################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "This forms the name of the security group. It is appended to the VPC name."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The security group description."
}

################################################################################
# Locals
################################################################################

locals {
  name = format("%s-%s", var.vpc, var.class)
}

################################################################################
# Resources
################################################################################

resource "aws_security_group" "scope" {
  name        = local.name
  vpc_id      = data.aws_vpc.scope.id
  description = var.description

  tags = {
    VPC     = var.vpc
    Name    = local.name
    Class   = var.class
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "id" {
  value = aws_security_group.scope.id
}

output "name" {
  value = aws_security_group.scope.name
}

output "tags" {
  value = aws_security_group.scope.tags_all
}

output "account_id" {
  value = aws_security_group.scope.owner_id
}

output "description" {
  value = aws_security_group.scope.description
}

################################################################################
