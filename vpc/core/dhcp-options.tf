################################################################################
# Optional Variables
################################################################################

variable "dhcp_options_class" {
  type        = string
  default     = "default"
  description = "This forms the name of the DHCP options. It is appended to the VPC name."
}

################################################################################

variable "dhcp_options_dns_servers" {
  type        = list(string)
  default     = ["AmazonProvidedDNS"]
  description = "List of DNS name servers."
}

################################################################################

variable "dhcp_options_domain_name" {
  type        = string
  default     = null
  description = "Allow the DHCP options domain name to be completely overridden."
}

variable "dhcp_options_domain_fallback_suffix" {
  type        = string
  default     = "compute.internal"
  description = "If 'route53_private_zone_create' is 'false', this is the DHCP options fallback domain suffix that is appended to the AWS region."
}

################################################################################

variable "dhcp_options_ipv6_lease_time" {
  type        = number
  default     = 86400
  description = "How frequently, in seconds, a running instance with an IPv6 assigned to it goes through DHCPv6 lease renewal."
}

################################################################################
# Locals
################################################################################

locals {
  dhcp_options_name        = format("%s-%s", var.name, var.dhcp_options_class)
  dhcp_options_domain_name = coalesce(var.dhcp_options_domain_name, join("", aws_route53_zone.private.*.name), format("%s.%s", data.aws_region.scope.name, var.dhcp_options_domain_fallback_suffix))
}

################################################################################
# Resources
################################################################################

resource "aws_vpc_dhcp_options" "scope" {
  domain_name                       = local.dhcp_options_domain_name
  domain_name_servers               = var.dhcp_options_dns_servers
  ipv6_address_preferred_lease_time = var.dhcp_options_ipv6_lease_time

  tags = {
    VPC     = var.name
    Name    = local.dhcp_options_name
    Owner   = var.owner
    Domain  = local.dhcp_options_domain_name
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################

resource "aws_vpc_dhcp_options_association" "scope" {
  vpc_id          = aws_vpc.scope.id
  dhcp_options_id = aws_vpc_dhcp_options.scope.id
}

################################################################################
# Outputs
################################################################################

output "dhcp_options_class" {
  value = var.dhcp_options_class
}

################################################################################

output "dhcp_options_domain_fallback_suffix" {
  value = var.dhcp_options_domain_fallback_suffix
}

################################################################################

output "dhcp_options_name" {
  value = local.dhcp_options_name
}

################################################################################

output "dhcp_options_id" {
  value = aws_vpc_dhcp_options.scope.id
}

output "dhcp_options_arn" {
  value = aws_vpc_dhcp_options.scope.arn
}

output "dhcp_options_dns_servers" {
  value = aws_vpc_dhcp_options.scope.domain_name_servers
}

output "dhcp_options_domain_name" {
  value = aws_vpc_dhcp_options.scope.domain_name
}

output "dhcp_options_ipv6_lease_time" {
  value = aws_vpc_dhcp_options.scope.ipv6_address_preferred_lease_time
}

################################################################################

output "dhcp_options_association_id" {
  value = aws_vpc_dhcp_options_association.scope.id
}

################################################################################
