################################################################################
# Optional Variables
################################################################################

variable "security_groups_any_port" {
  type        = number
  default     = 0
  description = "The port number that represents all ports. Applicable to egress rule in the perimeter security group."
}

################################################################################

variable "security_groups_icmp_mtu_type" {
  type        = number
  default     = 3
  description = "The ICMP type used for the MTU path discovery rule in the perimeter security group."
}

variable "security_groups_icmpv6_mtu_type" {
  type        = number
  default     = 2
  description = "The ICMPv6 type used for the MTU path discovery rule in the perimeter security group."
}

variable "security_groups_icmp_mtu_code" {
  type        = number
  default     = 4
  description = "The ICMP code used for the MTU path discovery rule in the perimeter security group."
}

variable "security_groups_icmpv6_mtu_code" {
  type        = number
  default     = 0
  description = "The ICMPv6 code used for the MTU path discovery rule in the perimeter security group."
}

variable "security_groups_icmp_echo_reply" {
  type        = number
  default     = 0
  description = "The ICMP type used for the echo replies rule in the perimeter security group."
}

variable "security_groups_icmpv6_echo_reply" {
  type        = number
  default     = 129
  description = "The ICMPv6 type used for the echo replies rule in the perimeter security group."
}

variable "security_groups_icmp_echo_request" {
  type        = number
  default     = 8
  description = "The ICMP type used for the echo requests rule in the perimeter security group."
}

variable "security_groups_icmpv6_echo_request" {
  type        = number
  default     = 128
  description = "The ICMPv6 type used for the echo requests rule in the perimeter security group."
}

variable "security_groups_icmp_echo_code" {
  type        = number
  default     = 0
  description = "The ICMP code used for the echo replies and echo requests rules in the perimeter security group."
}

variable "security_groups_icmpv6_echo_code" {
  type        = number
  default     = 0
  description = "The ICMPv6 code used for the echo replies and echo requests rules in the perimeter security group."
}

################################################################################

variable "security_groups_mtu_ipv4_cidrs" {
  type        = list(string)
  default     = ["0.0.0.0/0"]
  description = "List of IPv4 CIDR ranges from which MTU path discovery will be allowed."
}

variable "security_groups_mtu_ipv6_cidrs" {
  type        = list(string)
  default     = ["::/0"]
  description = "List of IPv6 CIDR ranges from which MTU path discovery will be allowed."
}

variable "security_groups_egress_ipv4_cidrs" {
  type        = list(string)
  default     = ["0.0.0.0/0"]
  description = "List of IPv4 CIDR ranges used for the egress rule in the perimeter security group."
}

variable "security_groups_egress_ipv6_cidrs" {
  type        = list(string)
  default     = ["::/0"]
  description = "List of IPv6 CIDR ranges used for the egress rule in the perimeter security group."
}

variable "security_groups_private_ipv4_cidrs" {
  type        = list(string)
  default     = []
  description = "List of IPv4 CIDR ranges that are internal to the network in addition to 'ipv4_cidr'. They are used in all rules (except egress) in the perimeter security group."
}

variable "security_groups_private_ipv6_cidrs" {
  type        = list(string)
  default     = []
  description = "List of IPv6 CIDR ranges that are internal to the network in addition to 'ipv4_cidr'. They are used in all rules (except egress) in the perimeter security group."
}

################################################################################

variable "security_groups_egress" {
  type        = string
  default     = "egress"
  description = "The keyword to mark a security group rule as egress. Should never be changed."
}

variable "security_groups_ingress" {
  type        = string
  default     = "ingress"
  description = "The keyword to mark a security group rule as ingress. Should never be changed."
}

################################################################################

variable "security_groups_any_protocol" {
  type        = string
  default     = "-1"
  description = "The keyword used for any protocol. Should never be changed."
}

variable "security_groups_icmp_protocol" {
  type        = string
  default     = "icmp"
  description = "The keyword used for ICMP protocol. Should never be changed."
}

variable "security_groups_icmpv6_protocol" {
  type        = string
  default     = "icmpv6"
  description = "The keyword used for ICMPv6 protocol. Should never be changed."
}

################################################################################

variable "security_groups_perimeter_class" {
  type        = string
  default     = "perimeter"
  description = "This forms the name of the perimeter security group. It is appended to the VPC name."
}

variable "security_groups_perimeter_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Description of the perimeter security group."
}

################################################################################

variable "security_groups_perimeter_mtu_description" {
  type        = string
  default     = null
  description = "Description of the MTU path discovery rule in the perimeter security group."
}

variable "security_groups_perimeter_echo_request_description" {
  type        = string
  default     = null
  description = "Description of the echo request rule in the perimeter security group."
}

variable "security_groups_perimeter_echo_reply_description" {
  type        = string
  default     = null
  description = "Description of the echo replies rule in the perimeter security group."
}

variable "security_groups_perimeter_egress_description" {
  type        = string
  default     = null
  description = "Description of the egress rule in the perimeter security group."
}

################################################################################
# Locals
################################################################################

locals {
  security_groups_perimeter_name                     = format("%s-%s", var.name, var.security_groups_perimeter_class)
  security_groups_private_ipv4_cidrs                 = concat(var.security_groups_private_ipv4_cidrs, tolist([aws_vpc.scope.cidr_block]))
  security_groups_private_ipv6_cidrs                 = concat(var.security_groups_private_ipv6_cidrs, tolist([aws_vpc.scope.ipv6_cidr_block]))
  security_groups_perimeter_mtu_description          = coalesce(var.security_groups_perimeter_mtu_description, var.security_groups_perimeter_description)
  security_groups_perimeter_echo_request_description = coalesce(var.security_groups_perimeter_echo_request_description, var.security_groups_perimeter_description)
  security_groups_perimeter_echo_reply_description   = coalesce(var.security_groups_perimeter_echo_reply_description, var.security_groups_perimeter_description)
  security_groups_perimeter_egress_description       = coalesce(var.security_groups_perimeter_egress_description, var.security_groups_perimeter_description)
}

################################################################################
# Resources
################################################################################

resource "aws_security_group" "perimeter" {
  vpc_id      = aws_vpc.scope.id
  name        = local.security_groups_perimeter_name
  description = var.security_groups_perimeter_description

  tags = {
    VPC     = var.name
    Name    = local.security_groups_perimeter_name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmp_path_mtu" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmp_protocol
  from_port         = var.security_groups_icmp_mtu_type
  to_port           = var.security_groups_icmp_mtu_code
  description       = local.security_groups_perimeter_mtu_description
  cidr_blocks       = var.security_groups_mtu_ipv4_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmpv6_path_mtu" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmpv6_protocol
  from_port         = var.security_groups_icmpv6_mtu_type
  to_port           = var.security_groups_icmpv6_mtu_code
  description       = local.security_groups_perimeter_mtu_description
  ipv6_cidr_blocks  = var.security_groups_mtu_ipv6_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmp_echo_request" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmp_protocol
  from_port         = var.security_groups_icmp_echo_request
  to_port           = var.security_groups_icmp_echo_code
  description       = local.security_groups_perimeter_echo_request_description
  cidr_blocks       = local.security_groups_private_ipv4_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmpv6_echo_request" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmpv6_protocol
  from_port         = var.security_groups_icmpv6_echo_request
  to_port           = var.security_groups_icmpv6_echo_code
  description       = local.security_groups_perimeter_echo_request_description
  ipv6_cidr_blocks  = local.security_groups_private_ipv6_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmp_echo_reply" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmp_protocol
  from_port         = var.security_groups_icmp_echo_reply
  to_port           = var.security_groups_icmp_echo_code
  description       = local.security_groups_perimeter_echo_reply_description
  cidr_blocks       = local.security_groups_private_ipv4_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_icmpv6_echo_reply" {
  type              = var.security_groups_ingress
  protocol          = var.security_groups_icmpv6_protocol
  from_port         = var.security_groups_icmpv6_echo_reply
  to_port           = var.security_groups_icmpv6_echo_code
  description       = local.security_groups_perimeter_echo_reply_description
  ipv6_cidr_blocks  = local.security_groups_private_ipv6_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################

resource "aws_security_group_rule" "perimeter_egress" {
  type              = var.security_groups_egress
  protocol          = var.security_groups_any_protocol
  from_port         = var.security_groups_any_port
  to_port           = var.security_groups_any_port
  description       = local.security_groups_perimeter_egress_description
  cidr_blocks       = var.security_groups_egress_ipv4_cidrs
  ipv6_cidr_blocks  = var.security_groups_egress_ipv6_cidrs
  security_group_id = aws_security_group.perimeter.id
}

################################################################################
# Outputs
################################################################################

output "security_groups_mtu_ipv4_cidrs" {
  value = var.security_groups_mtu_ipv4_cidrs
}

output "security_groups_mtu_ipv6_cidrs" {
  value = var.security_groups_mtu_ipv6_cidrs
}

output "security_groups_egress_ipv4_cidrs" {
  value = var.security_groups_egress_ipv4_cidrs
}

output "security_groups_egress_ipv6_cidrs" {
  value = var.security_groups_egress_ipv6_cidrs
}

output "security_groups_private_ipv4_cidrs" {
  value = local.security_groups_private_ipv4_cidrs
}

output "security_groups_private_ipv6_cidrs" {
  value = local.security_groups_private_ipv6_cidrs
}

################################################################################

output "security_groups_perimeter_class" {
  value = var.security_groups_perimeter_class
}

################################################################################

output "security_groups_perimeter_id" {
  value = aws_security_group.perimeter.id
}

output "security_groups_perimeter_name" {
  value = aws_security_group.perimeter.name
}

output "security_groups_perimeter_account_id" {
  value = aws_security_group.perimeter.owner_id
}

output "security_groups_perimeter_description" {
  value = aws_security_group.perimeter.description
}

################################################################################

output "security_groups_perimeter_rule_icmp_mtu_id" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.id
}

output "security_groups_perimeter_rule_icmp_mtu_type" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.type
}

output "security_groups_perimeter_rule_icmp_mtu_protocol" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.protocol
}

output "security_groups_perimeter_rule_icmp_mtu_icmp_type" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.from_port
}

output "security_groups_perimeter_rule_icmp_mtu_icmp_code" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.to_port
}

output "security_groups_perimeter_rule_icmp_mtu_description" {
  value = aws_security_group_rule.perimeter_icmp_path_mtu.description
}

################################################################################

output "security_groups_perimeter_rule_icmpv6_mtu_id" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.id
}

output "security_groups_perimeter_rule_icmpv6_mtu_type" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.type
}

output "security_groups_perimeter_rule_icmpv6_mtu_protocol" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.protocol
}

output "security_groups_perimeter_rule_icmpv6_mtu_icmp_type" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.from_port
}

output "security_groups_perimeter_rule_icmpv6_mtu_icmp_code" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.to_port
}

output "security_groups_perimeter_rule_icmpv6_mtu_description" {
  value = aws_security_group_rule.perimeter_icmpv6_path_mtu.description
}

################################################################################

output "security_groups_perimeter_rule_icmp_request_id" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.id
}

output "security_groups_perimeter_rule_icmp_request_type" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.type
}

output "security_groups_perimeter_rule_icmp_request_protocol" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.protocol
}

output "security_groups_perimeter_rule_icmp_request_icmp_type" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.from_port
}

output "security_groups_perimeter_rule_icmp_request_icmp_code" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.to_port
}

output "security_groups_perimeter_rule_icmp_request_description" {
  value = aws_security_group_rule.perimeter_icmp_echo_request.description
}

################################################################################

output "security_groups_perimeter_rule_icmpv6_request_id" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.id
}

output "security_groups_perimeter_rule_icmpv6_request_type" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.type
}

output "security_groups_perimeter_rule_icmpv6_request_protocol" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.protocol
}

output "security_groups_perimeter_rule_icmpv6_request_icmp_type" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.from_port
}

output "security_groups_perimeter_rule_icmpv6_request_icmp_code" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.to_port
}

output "security_groups_perimeter_rule_icmpv6_request_description" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_request.description
}

################################################################################

output "security_groups_perimeter_rule_icmp_reply_id" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.id
}

output "security_groups_perimeter_rule_icmp_reply_type" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.type
}

output "security_groups_perimeter_rule_icmp_reply_protocol" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.protocol
}

output "security_groups_perimeter_rule_icmp_reply_icmp_type" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.from_port
}

output "security_groups_perimeter_rule_icmp_reply_icmp_code" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.to_port
}

output "security_groups_perimeter_rule_icmp_reply_description" {
  value = aws_security_group_rule.perimeter_icmp_echo_reply.description
}

################################################################################

output "security_groups_perimeter_rule_icmpv6_reply_id" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.id
}

output "security_groups_perimeter_rule_icmpv6_reply_type" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.type
}

output "security_groups_perimeter_rule_icmpv6_reply_protocol" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.protocol
}

output "security_groups_perimeter_rule_icmpv6_reply_icmp_type" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.from_port
}

output "security_groups_perimeter_rule_icmpv6_reply_icmp_code" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.to_port
}

output "security_groups_perimeter_rule_icmpv6_reply_description" {
  value = aws_security_group_rule.perimeter_icmpv6_echo_reply.description
}

################################################################################

output "security_groups_perimeter_rule_egress_id" {
  value = aws_security_group_rule.perimeter_egress.id
}

output "security_groups_perimeter_rule_egress_type" {
  value = aws_security_group_rule.perimeter_egress.type
}

output "security_groups_perimeter_rule_egress_protocol" {
  value = aws_security_group_rule.perimeter_egress.protocol
}

output "security_groups_perimeter_rule_egress_port_from" {
  value = aws_security_group_rule.perimeter_egress.from_port
}

output "security_groups_perimeter_rule_egress_port_to" {
  value = aws_security_group_rule.perimeter_egress.to_port
}

output "security_groups_perimeter_rule_egress_description" {
  value = aws_security_group_rule.perimeter_egress.description
}

################################################################################
