<!----------------------------------------------------------------------------->

# vpc/core

#### The core components of a [VPC] with optional [Kubernetes] support

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//vpc/core`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_vpc_core" {
  source    = "gitlab.com/bitservices/network/aws//vpc/core"
  name      = "sandpit01"
  owner     = "terraform@bitservices.io"
  company   = "BITServices Ltd"
  ipv4_cidr = "192.168.188.0/23"
}
```

<!----------------------------------------------------------------------------->

[VPC]:        https://aws.amazon.com/vpc/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
