################################################################################
# Optional Variables
################################################################################

variable "nacl_default_class" {
  type        = string
  default     = "default"
  description = "This forms the name of the default NACL. It is appended to the VPC name."
}

################################################################################
# Locals
################################################################################

locals {
  nacl_default_name = format("%s-%s", var.name, var.nacl_default_class)
}

################################################################################
# Resources
################################################################################

resource "aws_default_network_acl" "scope" {
  default_network_acl_id = aws_vpc.scope.default_network_acl_id

  tags = {
    Name    = local.nacl_default_name
    VPC     = var.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "nacl_default_class" {
  value = var.nacl_default_class
}

################################################################################

output "nacl_default_name" {
  value = local.nacl_default_name
}

################################################################################

output "nacl_default_id" {
  value = aws_default_network_acl.scope.id
}

################################################################################
