################################################################################
# Optional Variables
################################################################################

variable "route53_private_zone_name" {
  type        = string
  default     = null
  description = "The full name of the VPC private Route 53 zone. This takes precedence over 'route53_private_zone_base'."
}

variable "route53_private_zone_base" {
  type        = string
  default     = "bitservices.io"
  description = "The base DNS domain name that 'name' should be prepended to in order to form the name of the VPC private Route 53 zone."
}

variable "route53_private_zone_type" {
  type        = string
  default     = "private"
  description = "The value to use for the 'Type' tag for the private Route 53 zone."
}

variable "route53_private_zone_create" {
  type        = bool
  default     = true
  description = "Whether or not the private Route 53 zone should be created for this VPC."
}

variable "route53_private_zone_auto_destroy" {
  type        = bool
  default     = true
  description = "Whether to automatically destroy all records (possibly managed outside of Terraform) in the private Route 53 zone for this VPC on destroy."
}

################################################################################
# Locals
################################################################################

locals {
  route53_private_zone_name = coalesce(var.route53_private_zone_name, format("%s.%s", var.name, var.route53_private_zone_base))
}

################################################################################
# Resources
################################################################################

resource "aws_route53_zone" "private" {
  count         = var.route53_private_zone_create ? 1 : 0
  name          = local.route53_private_zone_name
  force_destroy = var.route53_private_zone_auto_destroy

  tags = {
    VPC     = var.name
    Name    = local.route53_private_zone_name
    Type    = var.route53_private_zone_type
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  vpc {
    vpc_id     = aws_vpc.scope.id
    vpc_region = data.aws_region.scope.name
  }
}

################################################################################
# Outputs
################################################################################

output "route53_private_zone_base" {
  value = var.route53_private_zone_base
}

output "route53_private_zone_type" {
  value = var.route53_private_zone_type
}

output "route53_private_zone_create" {
  value = var.route53_private_zone_create
}

################################################################################

output "route53_private_zone_id" {
  value = length(aws_route53_zone.private) == 1 ? aws_route53_zone.private[0].zone_id : null
}

output "route53_private_zone_name" {
  value = length(aws_route53_zone.private) == 1 ? aws_route53_zone.private[0].name : null
}

output "route53_private_zone_auto_destroy" {
  value = length(aws_route53_zone.private) == 1 ? aws_route53_zone.private[0].force_destroy : null
}

output "route53_private_zone_name_servers" {
  value = length(aws_route53_zone.private) == 1 ? aws_route53_zone.private[0].name_servers : null
}

################################################################################
