################################################################################
# Optional Variables
################################################################################

variable "flow_log_class" {
  type        = string
  default     = "-flow-log"
  description = "This forms the name of the flow log. It is appended to the VPC name."
}

variable "flow_log_traffic" {
  type        = string
  default     = "REJECT"
  description = "The type of traffic to capture in the flow logs."
}

variable "flow_log_retention" {
  type        = number
  default     = 5
  description = "Specifies the number of days you want to retain the flow log events."
}

################################################################################

variable "flow_log_role_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the IAM role to be created for the management of this VPCs flow logs."
}

################################################################################
# Locals
################################################################################

locals {
  flow_log_name = format("%s%s", var.name, var.flow_log_class)
}

################################################################################
# Resources
################################################################################

resource "aws_flow_log" "scope" {
  vpc_id          = aws_vpc.scope.id
  traffic_type    = var.flow_log_traffic
  iam_role_arn    = aws_iam_role.scope.arn
  log_destination = aws_cloudwatch_log_group.scope.arn
}

################################################################################

resource "aws_cloudwatch_log_group" "scope" {
  name              = local.flow_log_name
  retention_in_days = var.flow_log_retention
}

################################################################################

resource "aws_iam_role" "scope" {
  name               = local.flow_log_name
  path               = var.flow_log_role_path
  assume_role_policy = <<EOF
{
  "Version"  : "2012-10-17",
  "Statement": [
    {
      "Sid"      : "1",
      "Effect"   : "Allow",
      "Action"   : "sts:AssumeRole",
      "Principal": { "Service": "vpc-flow-logs.amazonaws.com" }
    }
  ]
}
EOF
}

################################################################################

resource "aws_iam_role_policy" "scope" {
  name   = local.flow_log_name
  role   = aws_iam_role.scope.id
  policy = <<EOF
{
  "Version"  : "2012-10-17",
  "Statement": [
    {
      "Sid"   : "1",
      "Effect": "Allow",
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogStream",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "${aws_cloudwatch_log_group.scope.arn}:*"
      ]
    }
  ]
}
EOF
}

################################################################################
# Outputs
################################################################################

output "flow_log_vpc_id" {
  value = aws_flow_log.scope.vpc_id
}

output "flow_log_traffic" {
  value = aws_flow_log.scope.traffic_type
}

################################################################################

output "flow_log_group_arn" {
  value = aws_cloudwatch_log_group.scope.arn
}

output "flow_log_group_name" {
  value = aws_cloudwatch_log_group.scope.name
}

output "flow_log_group_retention" {
  value = aws_cloudwatch_log_group.scope.retention_in_days
}

################################################################################

output "flow_log_role_arn" {
  value = aws_iam_role.scope.arn
}

output "flow_log_role_name" {
  value = aws_iam_role.scope.name
}

output "flow_log_role_path" {
  value = aws_iam_role.scope.path
}

################################################################################

output "flow_log_policy_id" {
  value = aws_iam_role_policy.scope.id
}

output "flow_log_policy_name" {
  value = aws_iam_role_policy.scope.name
}

################################################################################
