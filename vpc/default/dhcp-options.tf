################################################################################
# Optional Variables
################################################################################

variable "dhcp_options_class" {
  type        = string
  default     = "default"
  description = "This forms the name of the DHCP options. It is appended to the default VPC name."
}

################################################################################
# Locals
################################################################################

locals {
  dhcp_options_name = format("%s-%s", var.name, var.dhcp_options_class)
}

################################################################################
# Resources
################################################################################

resource "aws_default_vpc_dhcp_options" "scope" {
  tags = {
    VPC     = var.name
    Name    = local.dhcp_options_name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "dhcp_options_class" {
  value = var.dhcp_options_class
}

################################################################################

output "dhcp_options_name" {
  value = local.dhcp_options_name
}

################################################################################
