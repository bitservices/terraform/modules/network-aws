################################################################################
# Optional Variables
################################################################################

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Name to be used for the default set of subnets."
}

variable "subnet_tier" {
  type        = string
  default     = "default"
  description = "Tier identifier for the default set of subnets."
}

################################################################################

variable "subnet_public_ip" {
  type        = bool
  default     = true
  description = "Specify 'true' to indicate that instances launched into the default subnets should be assigned a public IP address."
}

variable "subnet_assign_ipv6" {
  type        = bool
  default     = true
  description = "Specify 'true' to give all network interfaces launched into the default subnets an IPv6 address by default."
}

################################################################################
# Resources
################################################################################

resource "aws_default_subnet" "scope" {
  count                           = length(data.aws_availability_zones.scope.names)
  availability_zone               = data.aws_availability_zones.scope.names[count.index]
  map_public_ip_on_launch         = var.subnet_public_ip
  assign_ipv6_address_on_creation = var.subnet_assign_ipv6

  tags = {
    "Set"      = var.subnet_set
    "VPC"      = var.name
    "IPv6"     = var.subnet_assign_ipv6
    "Name"     = format("%s - %s - %s - %s", var.name, var.subnet_set, var.subnet_tier, replace(data.aws_availability_zones.scope.names[count.index], data.aws_region.scope.name, ""))
    "Tier"     = var.subnet_tier
    "Zone"     = replace(data.aws_availability_zones.scope.names[count.index], data.aws_region.scope.name, "")
    "Owner"    = var.owner
    "Region"   = data.aws_region.scope.name
    "Company"  = var.company
    "PublicIP" = var.subnet_public_ip
  }
}

################################################################################
# Outputs
################################################################################

output "subnet_set" {
  value = var.subnet_set
}

output "subnet_tier" {
  value = var.subnet_tier
}

################################################################################

output "subnet_public_ip" {
  value = var.subnet_public_ip
}

output "subnet_assign_ipv6" {
  value = var.subnet_assign_ipv6
}

################################################################################

output "subnet_ids" {
  value = aws_default_subnet.scope.*.id
}

output "subnet_zones" {
  value = aws_default_subnet.scope.*.availability_zone
}

output "subnet_ipv4_cidrs" {
  value = aws_default_subnet.scope.*.cidr_block
}

output "subnet_ipv6_cidrs" {
  value = aws_default_subnet.scope.*.ipv6_cidr_block
}

################################################################################
