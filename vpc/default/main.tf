################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = "default"
  description = "The full name of this default VPC."
}

variable "nau_metrics" {
  type        = bool
  default     = false
  description = "Indicates whether Network Address Usage metrics are enabled for your VPC."
}

################################################################################

variable "dns_support" {
  type        = bool
  default     = true
  description = "Whether or not the default VPC has DNS support."
}

variable "dns_hostnames" {
  type        = bool
  default     = true
  description = "Whether or not the default VPC has DNS hostname support."
}

################################################################################
# Resources
################################################################################

resource "aws_default_vpc" "scope" {
  enable_dns_support                   = var.dns_support
  enable_dns_hostnames                 = var.dns_hostnames
  enable_network_address_usage_metrics = var.nau_metrics
  assign_generated_ipv6_cidr_block     = true

  tags = {
    Name    = var.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "id" {
  value = aws_default_vpc.scope.id
}

output "name" {
  value = aws_default_vpc.scope.tags.Name
}

output "tenancy" {
  value = aws_default_vpc.scope.instance_tenancy
}

output "ipv4_cidr" {
  value = aws_default_vpc.scope.cidr_block
}

output "ipv6_cidr" {
  value = aws_default_vpc.scope.ipv6_cidr_block
}

output "dns_support" {
  value = aws_default_vpc.scope.enable_dns_support
}

output "nau_metrics" {
  value = aws_default_vpc.scope.enable_network_address_usage_metrics
}

output "dns_hostnames" {
  value = aws_default_vpc.scope.enable_dns_hostnames
}

output "ipv6_association_id" {
  value = aws_default_vpc.scope.ipv6_association_id
}

output "main_route_table_id" {
  value = aws_default_vpc.scope.main_route_table_id
}

output "default_route_table_id" {
  value = aws_default_vpc.scope.default_route_table_id
}

output "default_network_acl_id" {
  value = aws_default_vpc.scope.default_network_acl_id
}

output "default_security_group_id" {
  value = aws_default_vpc.scope.default_security_group_id
}

################################################################################
