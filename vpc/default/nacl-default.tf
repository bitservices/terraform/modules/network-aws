################################################################################
# Optional Variables
################################################################################

variable "nacl_default_ipv4_number" {
  type        = number
  default     = 32766
  description = "The rule number for the default IPv4 entries of the default NACL. NACL entries are processed in ascending order by rule number."
}

variable "nacl_default_ipv6_number" {
  type        = number
  default     = 32765
  description = "The rule number for the default IPv6 entries of the default NACL. NACL entries are processed in ascending order by rule number."
}

################################################################################

variable "nacl_default_class" {
  type        = string
  default     = "default"
  description = "This forms the name of the default NACL. It is appended to the default VPC name."
}

################################################################################

variable "nacl_default_ingress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default ingress rule for the default NACL table."
}

variable "nacl_default_ingress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default ingress rule for the 'default' NACL table. '-1' for any protocol."
}

variable "nacl_default_ingress_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "IPv4 CIDR range for the default ingress rule for the default NACL table."
}

variable "nacl_default_ingress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default ingress rule for the default NACL table."
}

variable "nacl_default_ingress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default ingress rule for the default NACL table."
}

variable "nacl_default_ingress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default ingress rule for the default NACL table."
}

################################################################################

variable "nacl_default_egress_action" {
  type        = string
  default     = "allow"
  description = "Action for the default egress rule for the default NACL table."
}

variable "nacl_default_egress_protocol" {
  type        = string
  default     = "-1"
  description = "Protocol for the default egress rule for the 'default' NACL table. '-1' for any protocol."
}

variable "nacl_default_egress_ipv4_cidr" {
  type        = string
  default     = "0.0.0.0/0"
  description = "IPv4 CIDR range for the default egress rule for the default NACL table."
}

variable "nacl_default_egress_ipv6_cidr" {
  type        = string
  default     = "::/0"
  description = "IPv6 CIDR range for the default egress rule for the default NACL table."
}

variable "nacl_default_egress_port_from" {
  type        = number
  default     = 0
  description = "Starting port for the default egress rule for the default NACL table."
}

variable "nacl_default_egress_port_to" {
  type        = number
  default     = 65535
  description = "Ending port for the default egress rule for the default NACL table."
}

################################################################################
# Locals
################################################################################

locals {
  nacl_default_name              = format("%s-%s", var.name, var.nacl_default_class)
  nacl_default_ingress_port_from = var.nacl_default_ingress_protocol == "-1" ? 0 : var.nacl_default_ingress_port_from
  nacl_default_ingress_port_to   = var.nacl_default_ingress_protocol == "-1" ? 0 : var.nacl_default_ingress_port_to
  nacl_default_egress_port_from  = var.nacl_default_egress_protocol == "-1" ? 0 : var.nacl_default_egress_port_from
  nacl_default_egress_port_to    = var.nacl_default_egress_protocol == "-1" ? 0 : var.nacl_default_egress_port_to
}

################################################################################
# Resources
################################################################################

resource "aws_default_network_acl" "scope" {
  subnet_ids             = aws_default_subnet.scope.*.id
  default_network_acl_id = aws_default_vpc.scope.default_network_acl_id

  tags = {
    VPC     = var.name
    Name    = local.nacl_default_name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }

  ingress {
    action     = var.nacl_default_ingress_action
    rule_no    = var.nacl_default_ipv4_number
    to_port    = local.nacl_default_ingress_port_to
    protocol   = var.nacl_default_ingress_protocol
    from_port  = local.nacl_default_ingress_port_from
    cidr_block = var.nacl_default_ingress_ipv4_cidr
  }

  ingress {
    action          = var.nacl_default_ingress_action
    rule_no         = var.nacl_default_ipv6_number
    to_port         = local.nacl_default_ingress_port_to
    protocol        = var.nacl_default_ingress_protocol
    from_port       = local.nacl_default_ingress_port_from
    ipv6_cidr_block = var.nacl_default_ingress_ipv6_cidr
  }

  egress {
    action     = var.nacl_default_egress_action
    rule_no    = var.nacl_default_ipv4_number
    to_port    = local.nacl_default_egress_port_to
    protocol   = var.nacl_default_egress_protocol
    from_port  = local.nacl_default_egress_port_from
    cidr_block = var.nacl_default_ingress_ipv4_cidr
  }

  egress {
    action          = var.nacl_default_egress_action
    rule_no         = var.nacl_default_ipv6_number
    to_port         = local.nacl_default_egress_port_to
    protocol        = var.nacl_default_egress_protocol
    from_port       = local.nacl_default_egress_port_from
    ipv6_cidr_block = var.nacl_default_egress_ipv6_cidr
  }
}

################################################################################
# Outputs
################################################################################

output "nacl_default_ipv4_number" {
  value = var.nacl_default_ipv4_number
}

output "nacl_default_ipv6_number" {
  value = var.nacl_default_ipv6_number
}

################################################################################

output "nacl_default_class" {
  value = var.nacl_default_class
}

################################################################################

output "nacl_default_ingress_action" {
  value = var.nacl_default_ingress_action
}

output "nacl_default_ingress_protocol" {
  value = var.nacl_default_ingress_protocol
}

output "nacl_default_ingress_ipv4_cidr" {
  value = var.nacl_default_ingress_ipv4_cidr
}

output "nacl_default_ingress_ipv6_cidr" {
  value = var.nacl_default_ingress_ipv6_cidr
}

output "nacl_default_ingress_port_from" {
  value = local.nacl_default_ingress_port_from
}

output "nacl_default_ingress_port_to" {
  value = local.nacl_default_ingress_port_to
}

################################################################################

output "nacl_default_egress_action" {
  value = var.nacl_default_egress_action
}

output "nacl_default_egress_protocol" {
  value = var.nacl_default_egress_protocol
}

output "nacl_default_egress_ipv4_cidr" {
  value = var.nacl_default_egress_ipv4_cidr
}

output "nacl_default_egress_ipv6_cidr" {
  value = var.nacl_default_egress_ipv6_cidr
}

output "nacl_default_egress_port_from" {
  value = local.nacl_default_egress_port_from
}

output "nacl_default_egress_port_to" {
  value = local.nacl_default_egress_port_to
}

################################################################################

output "nacl_default_name" {
  value = local.nacl_default_name
}

################################################################################

output "nacl_default_table_id" {
  value = aws_default_network_acl.scope.id
}

################################################################################
