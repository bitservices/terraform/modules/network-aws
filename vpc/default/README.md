<!----------------------------------------------------------------------------->

# vpc/default

#### Manage the *default* [VPC]

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/aws//vpc/default`**

--------------------------------------------------------------------------------

**NOTE:** Your *default* VPC and *default* subnets must have a **IPv6** CIDR
blocks attached in order to use this module. For details please see this guide:
[Default VPC and Default Subnets](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/default-vpc.html).

--------------------------------------------------------------------------------

### Example Usage

```
module "my_vpc_default" {
  source  = "gitlab.com/bitservices/network/aws//vpc/default"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[VPC]: https://aws.amazon.com/vpc/

<!----------------------------------------------------------------------------->
